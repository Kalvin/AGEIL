# coding: utf8
from django.db import models
from home.models import SemestrePossible, SemestreAnnualise, EtatSemestrePossible, Etudiant, StatutEtudiant, SemestreAnnualiseJuryEnCours, MoyenneUE, MoyenneSemestre, Association_Etudiant_SemestreAnnualise, DevenirEtudiantPossible, ModeleSemestre, AnneeUniversitaire, ResultatSemestrePossible
# Create your models here.

"""
	Cette classe va nous permettre de récupérer toutes les infos des 
	étudiants pour faciliter le trie et l'insertion dans le fichier Excel.
"""
class InfoEtuDoc:
	numApogee = ''
	nomEtu = ''
	prenomEtu = ''
	moySNMoins1 = None
	moySN = None
	moyUESNMoins1 = []
	moyUESN = []
	triAnnee = True

	resultatSNMoins1C = ''
	resultatSNMoins1PJ = ''
	resultatSNMoins1J = ''

	moyS1S2 = ''
	moyS2S3 = ''
	
	resultatSN = ''
	resultatSNPJ = ''
	#TODO voir s'il faut le resultat SNJ

	moyAnnee = None
	keyResultat = None
	
	"""
	Constructeur qui attend un étudiant et un semestre annualisé en paramètre.
	"""
	def __init__(self, etu, semestreAnnualise):
		#On traite ici les infos de la table Etudiant.
		self.numApogee = etu.numero
		self.nomEtu = etu.nom
		self.prenomEtu = etu.prenom
		
		"""
		Selon les semestres des semestreAnnualisées, il 
		va falloir faire des traitements différents. (comme récupérer le Semestre de 
		l'année dernière pour le S3 par exemple.)
		"""		
		
		if(semestreAnnualise.semestre.semestre == 'S2'):
			self.affecterSelonS2(etu, semestreAnnualise)
		if(semestreAnnualise.semestre.semestre == 'S3'):
			self.affecterSelonS3(etu, semestreAnnualise)
		if(semestreAnnualise.semestre.semestre == 'S4'):
			self.affecterSelonS4(etu, semestreAnnualise)
		#moy Annee
		
		moyAnnee = (self.moySNMoins1 + self.moySN) / 2
		
		self.moyAnnee = moyAnnee
		self.setkeyResultat()
		
	
	"""
	keyResultat va nous permettre de connaître l'ordre de trie, afin 
	de trier nos étudiants dans le bon ordre pour générer le document.
	"""
	def setkeyResultat(self):
		#Sn-1/Sn
		
		#VAL/VAL
		if self.resultatSNMoins1J == 'VAL' and self.resultatSN == 'VAL':
			self.keyResultat = 1 
		#VAL/VALC
		elif self.resultatSNMoins1J == 'VAL' and self.resultatSN == 'VALC':
			self.keyResultat = 2
		#VAL/NATT
		elif self.resultatSNMoins1J == 'VAL' and self.resultatSN == 'NATT':
			self.keyResultat = 3
		#VAL/NATB
		elif self.resultatSNMoins1J == 'VAL' and self.resultatSN == 'NATB':
			self.keyResultat = 4
				#VALC/VAL
		elif self.resultatSNMoins1J == 'VALC' and self.resultatSN == 'VAL':
			self.keyResultat = 5
		#NATT/NATT
		elif self.resultatSNMoins1J == 'NATT' and self.resultatSN == 'NATT':
			self.keyResultat = 6
		#NATT/NATB
		elif self.resultatSNMoins1J == 'NATT' and self.resultatSN == 'NATB':
			self.keyResultat = 7
		#NATB/NATB
		elif self.resultatSNMoins1J == 'NATB' and self.resultatSN == 'NATB':
			self.keyResultat = 8
		#DEF
		elif self.resultatSN == 'DEF':
			self.keyResultat = 10
		#Autres cas
		else:
			self.keyResultat = 9	
	"""
	Affectation des moyennes et des résultats ainsi que des UE S1/S2
	"""
	def affecterSelonS2(self, etu, semestreAnnualise):
		semestreAnnualiseSNMoins1 = SemestreAnnualise.objects.get(anneeUniversitaire=semestreAnnualise.anneeUniversitaire, semestre=SemestrePossible.objects.get(semestre='S1'))
		semestreAnnualiseSN = semestreAnnualise
			
		#moy S1
		moySemestreNmoins1 = MoyenneSemestre.objects.get(etudiant=etu, semestreAnnualise=semestreAnnualiseSNMoins1).moyenne
		#moy S2
		moySemestreN = MoyenneSemestre.objects.get(etudiant=etu, semestreAnnualise = semestreAnnualiseSN).moyenne
		#Resu S1
		resultatSemestreNmoins1C = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualiseSNMoins1).resultatSemestreCalcule.codeResultat
		resultatSemestreNmoins1PJ = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualiseSNMoins1).resultatSemestrePreJury.codeResultat
		resultatSemestreNmoins1J = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualiseSNMoins1).resultatSemestreJury.codeResultat
		#Resu S2
		resultatSemestreN = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualiseSN).resultatSemestreJury.codeResultat
		resultatSemestreNPJ = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualiseSN).resultatSemestrePreJury.codeResultat
		#Récupérer moyennes UE
		self.affecterUE(etu, semestreAnnualise, semestreAnnualiseSNMoins1)
		
		#MoyS1S2
		moyS1 = MoyenneSemestre.objects.get(etudiant=etu, semestreAnnualise=semestreAnnualiseSNMoins1).moyenne
		self.moyS1S2 = (moyS1 + moySemestreNmoins1) / 2
		
		self.affecterMoyenneEtResu(moySemestreNmoins1, moySemestreN, resultatSemestreNmoins1C, resultatSemestreNmoins1PJ, resultatSemestreNmoins1J, resultatSemestreN, resultatSemestreNPJ)
	"""
	Affectation des moyennes, des résultats et des UE du S3 XXXX/YYYY
	puis des moyennes, des resultats et des UE du S2 XXXX-1/YYYY-1
	"""
	def affecterSelonS3(self, etu, semestreAnnualise):
		#L'année universitaire du semestre annualisé.
		anneeUniversitaireActuel = semestreAnnualise.anneeUniversitaire.anneeUniversitaire
		#On récupère les 2 années et on les diminue de 1 pour avoir le S2 de l'année dernière.
		annee1,annee2 = anneeUniversitaireActuel.split('/')
		annee1 = int(annee1) - 1
		annee2 = int(annee2) - 1
		anneeUniversitaireNmoins1String = str(annee1) + '/' + str(annee2)
		anneeUniversitaireNmoins1 = AnneeUniversitaire.objects.get(anneeUniversitaire=anneeUniversitaireNmoins1String)
		semestreAnnualiseSNMoins1 = SemestreAnnualise.objects.get(anneeUniversitaire=anneeUniversitaireNmoins1, semestre=SemestrePossible.objects.get(semestre='S2'))
		
		#Moy S2
		moySemestreNmoins1 = MoyenneSemestre.objects.get(etudiant=etu, semestreAnnualise=semestreAnnualiseSNMoins1).moyenne		
		
		#MoyS1S2
		semestreAnnualiseS1 = SemestreAnnualise.objects.get(anneeUniversitaire=anneeUniversitaireNmoins1, semestre=SemestrePossible.objects.get(semestre='S1'))
		moyS1 = MoyenneSemestre.objects.get(etudiant=etu, semestreAnnualise=semestreAnnualiseS1).moyenne
		self.moyS1S2 = (moyS1 + moySemestreNmoins1) / 2
		
		#MoyS3
		moySemestreN = MoyenneSemestre.objects.get(etudiant=etu, semestreAnnualise = semestreAnnualise).moyenne	

		#MoyS2S3
		self.moyS2S3 = (moySemestreNmoins1 + moySemestreN) / 2
		
		
		#Resu S2
		resultatSemestreNmoins1C = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualiseSNMoins1).resultatSemestreCalcule.codeResultat
		resultatSemestreNmoins1PJ = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualiseSNMoins1).resultatSemestrePreJury.codeResultat
		resultatSemestreNmoins1J = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualiseSNMoins1).resultatSemestreJury.codeResultat
		#Resu S2
		resultatSemestreN = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualise).resultatSemestreJury.codeResultat
		resultatSemestreNPJ = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualise).resultatSemestrePreJury.codeResultat
		
		#Affectations des UEs
		self.affecterUE(etu, semestreAnnualise, semestreAnnualiseSNMoins1)
		
		self.affecterMoyenneEtResu(moySemestreNmoins1, moySemestreN, resultatSemestreNmoins1C, resultatSemestreNmoins1PJ, resultatSemestreNmoins1J, resultatSemestreN, resultatSemestreNPJ)
	
	"""
	Affectations des moyennes, des résultats et des UE du S3/S4
	"""
	def affecterSelonS4(self, etu, semestreAnnualise):
		semestreAnnualiseS3 = SemestreAnnualise.objects.get(anneeUniversitaire=semestreAnnualise.anneeUniversitaire, semestre=SemestrePossible.objects.get(semestre='S3'))
		#moy S3
		moySemestreNmoins1 = MoyenneSemestre.objects.get(etudiant=etu, semestreAnnualise=semestreAnnualiseS3).moyenne
		#moy S4
		moySemestreN = MoyenneSemestre.objects.get(etudiant=etu, semestreAnnualise = semestreAnnualise).moyenne
		#Resu S3
		resultatSemestreNmoins1C = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualiseS3).resultatSemestreCalcule.codeResultat
		resultatSemestreNmoins1PJ = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualiseS3).resultatSemestrePreJury.codeResultat
		resultatSemestreNmoins1J = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualiseS3).resultatSemestreJury.codeResultat
		#Resu S4
		resultatSemestreN = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualise).resultatSemestreCalcule.codeResultat
		resultatSemestreNPJ = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualise).resultatSemestrePreJury.codeResultat
		
		"""
		TODO 
		voir s'il faut générer le resultat pour une 3ème génération 
		avec les résultat du jury au semestre N dans l'application.
		"""
		#resultatSemestreNJ = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=semestreAnnualise).resultatSemestreJury.codeResultat
		
		#Affectations des UEs
		self.affecterUE(etu, semestreAnnualise, semestreAnnualiseS3)
		
		self.affecterMoyenneEtResu(moySemestreNmoins1, moySemestreN, resultatSemestreNmoins1C, resultatSemestreNmoins1PJ, resultatSemestreNmoins1J, resultatSemestreN, resultatSemestreNPJ)
	"""
	Affecte les moyennes des UE de l'étudiant à notre classe.
	Paramètre attendu : l'étudiant, le semestreAnnualise à l'année N
	et le semestre annualisé à l'année N-1.
	"""
	def affecterUE(self, etu, semestreAnnualise, semestreAnnualiseSNMoins1):
		#Ajout des moyennes de l'UE du SN
		self.moyUESN = []
		tabUE = None
		#On récupère les UE du semestreAnnualise actuel
		tabUE = MoyenneUE.objects.filter(etudiant = etu, semestreAnnualise = semestreAnnualise)
		#On ajoute les moyennes des UE
		for UE in tabUE:
			self.moyUESN.append(UE.moyenne)
		
		#Ajout des moyennes de l'UE du SNMoins1
		self.moyUESNMoins1 = []
		tabUE = None
		#On récupère les UE du semestre annualisé de l'an dernier
		tabUE = MoyenneUE.objects.filter(etudiant = etu, semestreAnnualise = semestreAnnualiseSNMoins1)
		#On ajoute les moyennes des UE
		for UE in tabUE:
			self.moyUESNMoins1.append(UE.moyenne)
	
	"""
	Permet d'affecter les moyenne et résultat au semestre N,
	et au semestre N-1.
	Paramètre attendu :
		-moySemestreNmoins1 : La moyenne du semestre N-1 (ex : moyenne S1)
		-moySemestreN : La moyenne du semestre N (ex : moyenne S2)
		-resultatSemestreNmoins1C : Le résultat calculé au S N-1 (S1)
		-resultatSemestreNmoins1PJ : Le résultat pré-jury au S N-1 (S1)
		-resultatSemestreNmoins1J : Le résultat jury au S N-1 (S1)
		-resultatSemestreNmoins1 : (ex : VAL pour le S1)
		-resultatSemestreN : (ex : VAL pour le S2)
		-resultatSemestreNPJ : Le résultat jury au SN (S2)
	"""
	def affecterMoyenneEtResu(self, moySemestreNmoins1, moySemestreN, resultatSemestreNmoins1C, resultatSemestreNmoins1PJ, resultatSemestreNmoins1J, resultatSemestreN, resultatSemestreNPJ):
		"""
		Si le résultat calculé est ATTENTE_DE_DECISION alors 
		c'est que le jury n'a pas décidé ce qu'il allait faire de l'étudiant, on met le champ vide.
		Par exemple : ceux qui ont été décidé NATT ou NATB par le jury.
		"""
		if(resultatSemestreNmoins1C == 'ATTENTE_DE_DECISION'):
			self.resultatSNMoins1C = ''
		else:
			self.resultatSNMoins1C = resultatSemestreNmoins1C
		if(resultatSemestreNmoins1PJ == 'ATTENTE_DE_DECISION'):
			self.resultatSNMoins1PJ = ''
		else:
			self.resultatSNMoins1PJ = resultatSemestreNmoins1PJ
		if(resultatSemestreNmoins1J == 'ATTENTE_DE_DECISION'):
			self.resultatSNMoins1J = ''
		else:
			self.resultatSNMoins1J = resultatSemestreNmoins1J
		if(resultatSemestreN == 'ATTENTE_DE_DECISION'):
			self.resultatSN = ''
		else:
			self.resultatSN = resultatSemestreN
		if(resultatSemestreNPJ == 'ATTENTE_DE_DECISION'):
			self.resultatSNPJ = ''
		else:
			self.resultatSNPJ = resultatSemestreNPJ
		
		self.moySNMoins1 = moySemestreNmoins1
		self.moySN = moySemestreN
	"""
	toString()
	"""
	
	def __str__(self):
		string = 'Numero Apogee : ' + str(self.numApogee) + ', Nom : ' + str(self.nomEtu) + ', Prenom : ' + str(self.prenomEtu) + ', moyenne SN-1 : ' + str(self.moySNMoins1) + ', Moyenne SN : ' + str(self.moySN) + ', Moyenne Annee : ' + str(self.moyAnnee) + ', Resultat SN-1 : ' + str(self.resultatSNMoins1C) + ' ' + str(self.resultatSNMoins1PJ) + ' '+  str(self.resultatSNMoins1J) + ' ' + ', Resultat SN : ' + str(self.resultatSN)
		i = 1
		for moy in self.moyUESN:
			string += 'Moyenne UE' + str(i) + ' (SN-1) : ' + str(moy)
			i += 1
		for moy in self.moyUESNMoins1:
			string += 'Moyenne UE' + str(i) + ' (SN) : ' + str(moy)
			i += 1
		return string
	
	"""
	Ce qui nous sert à trier.
	"""
	def __repr__(self):
		return repr((self.numApogee,self.moySN,selfmoyAnnee,self.keyResultat))
