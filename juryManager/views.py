# coding: utf8 with BOM
from django.shortcuts import render
from home.models import SemestrePossible, SemestreAnnualise, EtatSemestrePossible, Etudiant, StatutEtudiant, SemestreAnnualiseJuryEnCours, MoyenneUE, MoyenneSemestre, Association_Etudiant_SemestreAnnualise, DevenirEtudiantPossible, ModeleSemestre, AnneeUniversitaire, ResultatSemestrePossible
from home.forms import Association_Etudiant_SemestreAnnualiseForm
import os
from juryManager.models import InfoEtuDoc

#Librairie openpyxl
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.styles import Font, Alignment, Border, Side

# Create your views here.
import datetime
from datetime import date

#Constantes utilisables dans toutes les fonctions.
DEP = 'Département GMP'
C_NUMERO = 'A'
C_NAPOGEE = 'B'
C_NOM = 'C'
C_PRE = 'D'
CELL_ANNEE_UNIVERSITAIRE = 'X2'
#TODO à mettre en place pour avoir une trace du document.
CELL_DATE = 'X4'

def jury(request):
	if request.method == 'POST':
		semestreChoisi = request.POST['semestreChoisi']
		#On récupère la date du doc.
		dateChoisie = request.POST.get('dateChoisie')
		dateChoisie = getDateFrancais(dateChoisie)

		trieChoisie = request.POST.get('trie')
		if(trieChoisie == 'trieSN'):
			triAnnee = False
		else:
			triAnnee = True

		semestre = SemestreAnnualise.objects.get(id=semestreChoisi)
		numeroSemestre = int(semestre.semestre.semestre[1])

		#La chaine de caractère semestre.
		S = semestre.semestre.semestre

		"""
		TODO penser à rajouter des cases au tableau si jamais y'a + d'étudiants que de cases !!!
		"""

		"""
		TODO écrire les étudiants pour le SN dans le fic excel !!
		"""

		#On récupère tous les étudiants du semestre selectionné.
		etudiants = Etudiant.objects.filter(semestreActuel=semestre)

		today = date.today()

		#Chargement du fichier selon le semestre choisit. True pour avoir une maquette SN-1/SN
		wb = choisirFichierSelonSemestre(semestre.semestre.semestre, True)

		#On trie les étudiants dans un tableau.
		tabEtuTrier = trierEtudiants(etudiants, semestre, triAnnee)

		try:
			if(S == 'S2'):
				#False car on ne veut pas le résultat pré-jury
				fichier = ecrireSelonS1S2(False, tabEtuTrier, semestre, wb, dateChoisie)
			if(S == 'S3'):
				fichier = ecrireSelonS2S3(False, tabEtuTrier, semestre, wb, dateChoisie)
			if(S == 'S4'):
				fichier = ecrireSelonS3S4(False, tabEtuTrier, semestre, wb, dateChoisie)
		except:
			# erreur
			titreErreur = 'Erreur dans la génération du fichier !'
			messageErreur = 'AGEIL n\'arrive pas à générer le fichier attendu.'
			detailsErreur = []
			detailsErreur.append('Vérifiez que le fichier n\'est pas déjà généré et utiliser par un autre processus.')
			detailsErreur.append('Peut-être que vous le fichier est déjà ouvert par excel et qu\'il ne peut pas être écrasé.')
			return render(request, 'erreurUtilisateur.html', locals())
		semestre.etatSemestre = EtatSemestrePossible.objects.get(etat='DOC_JURY_GENERE')
		semestre.save()
	
	semestreAnnualises = SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='DOC_PRE_JURY_GENERE')).reverse()
	semestreAnnualises = semestreAnnualises | SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='DOC_JURY_GENERE')).reverse()
	semestreAnnualises = semestreAnnualises | SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='DOC_PV_GENERE')).reverse()
	return render(request,'juryManager/jury.html',locals())

"""
view appelé pour gérer la page préjury.
"""
def preJury(request):
	if request.method == 'POST':
		semestreChoisi = request.POST['semestreChoisi']
		#On récupère la date du doc.
		dateChoisie = request.POST.get('dateChoisie')
		dateChoisie = getDateFrancais(dateChoisie)
		triAnnee = True

		tri = request.POST['trie']
		if tri == 'trieSN':
			triAnnee = False
		else:
			triAnnee = True

		semestre = SemestreAnnualise.objects.get(id=semestreChoisi)
		numeroSemestre = int(semestre.semestre.semestre[1])


		#La chaine de caractère semestre.
		S = semestre.semestre.semestre

		#On récupère tous les étudiants du semestre selectionné.
		etudiants = Etudiant.objects.filter(semestreActuel=semestre)

		today = date.today()

		#Chargement du fichier selon le semestre choisi. True pour avoir maquette SN-1/SN
		wb = choisirFichierSelonSemestre(semestre.semestre.semestre, True)

		#On trie les étudiants dans un tableau.
		try :
			tabEtuTrier = trierEtudiants(etudiants, semestre, triAnnee)
		except:
			#erreur
			titreErreur = 'Erreur dans la génération du fichier'
			messageErreur = 'Impossible de trier les étudiants.'
			detailsErreur = []
			detailsErreur.append('Il est possible qu\'un étudiant n\'est pas de moyenne pour ce semestre ;')
			detailsErreur.append('Vérifiez que tous les étudiants ont une moyenne pour le semestre ' + str(semestre) + ' ;')
			detailsErreur.append('Vérifiez si tous les étudiants présents dans l\'application pour le semestre ' + str(semestre) + ' étaient bien présents dans le fichier CSV contenant les notes ;')
			detailsErreur.append('Si vous avez oublié de placer un étudiant à l\'état réorienté au semestre précédent, vous risquez de devoir le supprimer via le menu "gestion des étudiants" > bouton "Lister les étudiants" > bouton "Consulter l\'étudiant" concerné > bouton "Supprimer l\'étudiant".')
			return render(request, 'erreurUtilisateur.html', locals())
		try:
			if(S == 'S2'):
				#True car on ne veut pas le résultat pré-jury
				fichier = ecrireSelonS1S2(True, tabEtuTrier, semestre, wb, dateChoisie)
			if(S == 'S3'):
				fichier = ecrireSelonS2S3(True, tabEtuTrier, semestre, wb, dateChoisie)
			if(S == 'S4'):
				fichier = ecrireSelonS3S4(True, tabEtuTrier, semestre, wb, dateChoisie)
		except:
			# erreur
			titreErreur = 'Erreur dans la génération du fichier'
			messageErreur = 'AGEIL n\'arrive pas à générer le fichier attendu.'
			detailsErreur = []
			detailsErreur.append('Vérifiez que le fichier n\'est pas déjà généré et utiliser par un autre processus.')
			detailsErreur.append('Peut-être que le fichier est déjà ouvert par excel et qu\'il ne peut pas être écrasé.')
			return render(request, 'erreurUtilisateur.html', locals())

		semestre.etatSemestre = EtatSemestrePossible.objects.get(etat='DOC_PRE_JURY_GENERE')
		semestre.save()

	semestreAnnualises = SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='MOYENNES_IMPORTEES')).reverse()
	semestreAnnualises = semestreAnnualises | SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='DOC_PRE_JURY_GENERE')).reverse()
	semestreAnnualises = semestreAnnualises | SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='DOC_JURY_GENERE')).reverse()
	semestreAnnualises = semestreAnnualises | SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='DOC_PV_GENERE')).reverse()
	return render(request,'juryManager/preJury.html',locals())

"""
View appelé lors de la page générer un PV.
"""
def pv(request):
	BASE_DIR = os.path.dirname(os.path.abspath(__file__))

	if request.method == 'POST':
		semestreChoisi = request.POST['semestreChoisi']
		dateChoisie = request.POST.get('dateChoisie')

		semestre = SemestreAnnualise.objects.get(id=semestreChoisi)
		numeroSemestre = int(semestre.semestre.semestre[1])


		dirJury = request.POST.get('dirJury')

		dateChoisie = getDateFrancais(dateChoisie)

		#La chaine de caractère semestre.
		S = semestre.semestre.semestre

		"""
		TODO écrire ces méthodes.
		"""

		#Selon le semestre, on va générer le bon PV :
		if(S == 'S1'):
			fichier = genererPVS1(dirJury, semestre, dateChoisie)
		if(S == 'S2'):
			fichier = genererPVS2(dirJury, semestre, dateChoisie)
		if(S == 'S3'):
			fichier = genererPVS3(dirJury, semestre, dateChoisie)
		if(S == 'S4'):
			fichier = genererPVS4(dirJury, semestre, dateChoisie)

		semestre.etatSemestre = EtatSemestrePossible.objects.get(etat='DOC_PV_GENERE')
		semestre.save()
	
	semestreAnnualises = SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='DOC_JURY_GENERE')).reverse()
	semestreAnnualises = semestreAnnualises | SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='MOYENNES_IMPORTEES'), semestre=SemestrePossible.objects.get(semestre='S1')).reverse()
	semestreAnnualises = semestreAnnualises | SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='DOC_PV_GENERE')).reverse()
	return render(request,'juryManager/PV.html',locals())
"""
Retourne la date au format français (ex : 19 janvier 2017)
Paramètre :
	-dateChoisie : La date à convertir en français.
"""
def getDateFrancais(dateChoisie):
	today = date.today()
	dateToday = str(today.day) + ' ' + dictMois()[today.month] + ' ' + str(today.year)
	try:
		annee, mois, jour = dateChoisie.split('-')
		dateChoisie = jour + ' ' + dictMois()[int(mois)] + ' ' + annee
	except:
		#On gère le cas où l'utilisateur rentre jj/mm/aaaa
		try:
			dateChoisie = dateChoisie.replace('/','-')
			jour, mois, annee = dateChoisie.split('-')
			dateChoisie = jour + ' ' + dictMois()[int(mois)] + ' ' + annee
		#En cas de mauvaises saisie, on utilise la date par défaut.
		except:
			dateChoisie = dateToday
	return dateChoisie
"""
Retourne un tableau associatif des différents mois

"""
def dictMois():
	return {1:'Janvier',2:'Février',3:'Mars',4:'Avril',5:'Mai',6:'Juin',7:'Juillet',8:'Aout',9:'Septembre',10:'Octobre',11:'Novembre',12:'Décembre'}

"""
Retourne un tablau associatif des différents codes de validation
"""
def dictValidation():
	return {'VAL':'VAL','VALJ':'VAL','VALC':'VAL','ADAC':'VAL','NVAL':'NVAL','AJPC':'NVAL','DEF':'NVAL','NATB':'NVAL','NATT':'NVAL','ATTENTE_DE_DECISION':''}

"""
Retourne une liste d'étudiants en fonction d'un semestre annualisé
Paramètre :
	- Semestre annualise
"""
def getEtudiant(semestreAnnualise):
	return Association_Etudiant_SemestreAnnualise.objects.filter(semestre = semestreAnnualise)

"""
Retourne une liste d'étudiants ayant validés un semestre
Paramètre :
	- Semestre annualise
"""
def getEtudiantValide(semestreAnnualise):
	sv = ResultatSemestrePossible.objects.get(codeResultat = 'VAL')
	return Association_Etudiant_SemestreAnnualise.objects.filter(semestre = semestreAnnualise).filter(resultatSemestreJury = sv).order_by('etudiant')

"""
Retourne une liste d'étudiants n'ayant pas validé un semestre
Paramètre :
	- Semestre annualise
"""
def getEtudiantNonValide(semestreAnnualise):
	snatt = ResultatSemestrePossible.objects.get(codeResultat = 'NATT')
	snatb = ResultatSemestrePossible.objects.get(codeResultat = 'NATB')
	return Association_Etudiant_SemestreAnnualise.objects.filter(semestre = semestreAnnualise).filter(resultatSemestreCalcule = snatb).filter(resultatSemestreCalcule = snatb).order_by('etudiant')

"""
Ecrit dans la feuille excel les étudiants ayant capitalisées leurs UEs
Paramètre :
	- Semestre annualise
	- ws : WorkSheet
	- COL_NOM : colonne ou doit se trouver le nom
	- COL_PRE : colonne ou doit de trouver le prenom
	- COL_UE_CAP : colonne ou doit se trouver les UEs
	- ligne : ligne ou doit commencer l'écriture
	- merge : colonne jusqu'ou fusionner les données des UEs capitalisées
Retourne le numéro de la ligne ou l'écriture s'arrête
"""
def ecrireUECapitalisees(semestreAnnualise, ws, COL_NOM, COL_PRE, COL_UE_CAP, ligne, merge):
	enTeteEcrit = False

	for etu in getEtudiantNonValide(semestreAnnualise):
		if enTeteEcrit == False:
			#Ecrit l'en tete
			ws['A' + str(ligne)] = 'Etudiants n\'ayant pas validé le Semestre 1 mais ayant des UEs capitalisées'
			ws['A' + str(ligne)].font = Font(underline="single")

			ligne += 2

			ws[COL_NOM + str(ligne)] = 'NOM'
			ws[COL_NOM + str(ligne)].font = Font(bold=True)
			ws[COL_NOM + str(ligne)].border = Border(top=Side(style='thin'), bottom=Side(style='thin'), left=Side(style='thin'), right=Side(style='thin'))

			ws[COL_PRE + str(ligne)] = 'PRENOM'
			ws[COL_PRE + str(ligne)].font = Font(bold=True)
			ws[COL_PRE + str(ligne)].border = Border(top=Side(style='thin'), bottom=Side(style='thin'), right=Side(style='thin'))

			ws.merge_cells(COL_UE_CAP + str(ligne)+':'+merge+str(ligne))
			ws[COL_UE_CAP + str(ligne)] = 'UE CAPITALISEES'
			ws[COL_UE_CAP + str(ligne)].font = Font(bold=True)
			ws[COL_UE_CAP + str(ligne)].border = Border(bottom=Side(style='thin'), right=Side(style='thin'), left=Side(style='thin'), top=Side(style='thin'))
			ws[merge+str(ligne)].border = Border(bottom=Side(style='thin'), right=Side(style='thin'), left=Side(style='thin'), top=Side(style='thin'))

			ligne += 1
			enTeteEcrit = True


		et = etu.etudiant
		UEs = MoyenneUE.objects.filter(etudiant = et)

		UEsCap = False

		for UE in UEs:
			if(UE.moyenne >= 10):
				UEsCap = True

		if len(UEs) != 0:
			if UEsCap:
				ws[COL_NOM + str(ligne)] = etu.etudiant.nom
				ws[COL_NOM + str(ligne)].border = Border(bottom=Side(style='thin'), right=Side(style='thin'), left=Side(style='thin'), top=Side(style='thin'))

				ws[COL_PRE + str(ligne)] = etu.etudiant.prenom
				ws[COL_PRE + str(ligne)].border = Border(bottom=Side(style='thin'), right=Side(style='thin'), left=Side(style='thin'), top=Side(style='thin'))

				sUEs = ''
				for UE in UEs:

					if(UE.moyenne >= 10):
						print (UE)
						sUEs = sUEs + UE.modeleUE.code + ', '
				ws.merge_cells(COL_UE_CAP + str(ligne)+':'+merge+str(ligne))
				ws[chr(ord(COL_UE_CAP)) + str(ligne)] = sUEs
				ws[COL_UE_CAP + str(ligne)].border = Border(bottom=Side(style='thin'), right=Side(style='thin'), left=Side(style='thin'), top=Side(style='thin'))
				ws[merge+str(ligne)].border = Border(bottom=Side(style='thin'), right=Side(style='thin'), left=Side(style='thin'), top=Side(style='thin'))
				ligne +=1
	return ligne
"""
Ecrit le footer du PV
	- ws : feuille à remplir
	- dateChoisi : date choisie
	- COL_CENTRE : cellule du centre de la feuille
	- ligne : ligne ou commencer à écrire
	- DIR_IUT : nom apparant dans le footer
"""

def ecrireFooterPV(ws, dateChoisi, COL_CENTRE, ligne, DIR_IUT):
	#TODO Si possible mettre sous la forme 28 aout 2011
	ws[COL_CENTRE + str(ligne)] = 'Limoges, le ' + dateChoisi
	ws[COL_CENTRE + str(ligne)].alignment = Alignment(horizontal='center')

	ligne += 2

	ws[COL_CENTRE + str(ligne)] = 'Le président du Jury'
	ws[COL_CENTRE + str(ligne)].alignment = Alignment(horizontal='center')

	ligne += 2

	ws[COL_CENTRE + str(ligne)] = DIR_IUT
	ws[COL_CENTRE + str(ligne)].alignment = Alignment(horizontal='center')

"""
Permet de trier un tableau d'étudiant pour qu'il soit trié selon l'ordre du document.
Pour le moment le trie se fait UNIQUEMENT sur la moyenne des 2 semestres !!!
Il faut permettre à l'utilisateur de sélectionner la moyenne sur laquelle il veut que
le trise se fasse. 2 choix possibles soit on se base sur le SN soit sur la moyenne des 2.

Paramètres :
	-etudiants : Un tableau d'étudiant.
	-semestre : Un semestreAnnualise.
	-triAnnee : Si true trie par moyenne année, trie par moyenne dernier semestre sinon.
"""
def trierEtudiants(etudiants, semestre, triAnnee):
	tabEtuTrier = []
	for etu in etudiants:
		infoEtu = None
		infoEtu = InfoEtuDoc(etu, semestre)

		tabEtuTrier.append(infoEtu)


	#Choix du trie par la moyenne.
	if(triAnnee):
		tabEtuTrier = sorted(tabEtuTrier,reverse=True, key=lambda e: e.moyAnnee)
	else:
		tabEtuTrier = sorted(tabEtuTrier,reverse=True, key=lambda e: e.moySN)
	tabEtuTrier = sorted(tabEtuTrier,key=lambda e: e.keyResultat)

	return tabEtuTrier

"""
Permet de définir le résultat Etape
Paramètre :
	-annee : 1 ou 2 Si 1 c'est prmeière année si 2 c'est deuxième année.
	-resN : Le resultat du semestre N.
	-resNMoins1 : Le resultat du semestre N-1.
"""
def calculerResEtape(annee, resN, resNMoins1):
	if annee == 2:
		#VAL/VAL
		if dictValidation()[resN] == 'VAL' and dictValidation()[resNMoins1] == 'VAL':
			return 'ADM'
		#Autres cas
		else:
			return 'AJ'
	elif annee == 1:
		#VAL/VAL
		if (resN == 'VAL' or resN == 'VALJ') and (resNMoins1 == 'VAL' or resNMoins1 == 'VALJ'):
			return 'ADM'
		#VAL/VALC
		if resN == 'VAL' and resNMoins1 == 'VALC':
			return 'ADMC'
		#ADAC/VAL
		if resN == 'ADAC' and resNMoins1 == 'VAL':
			return 'ADMC'
		#VAL/NVAL
		if (resN == 'VAL' or resN == 'VALJ') and (resNMoins1 == 'NATT' or resNMoins1 == 'NATB' or resNMoins1 == 'NVAL'):
			return 'AJAC'

		#NVAL/NVAL
		if (resN == 'AJPC' or resN == 'NATT' or resN == 'NATB' or resN == 'NVAL') and  (resNMoins1 == 'NATT' or resNMoins1 == 'NATB' or resNMoins1 == 'NVAL'):
			return 'AJ'
	else:
		# erreur
		titreErreur = 'Erreur lors du calcule du résultat étape.'
		messageErreur = 'Impossible de calculer le résultat étape !'
		detailsErreur = []
		detailsErreur.append('Vérifiez que les résultats sont bien attribués pour S1S2')
		detailsErreur.append('Attention le calcule étape pour une troisième année n\'est pas géré par l\'application.')
		return render(request, 'erreurUtilisateur.html', locals())


"""
Va permettre d'écrire dans le fichier Selon S1S2
Paramètre :
	-preJury : Booleen
		-Si true on ne génère pas le résultat préjury, on génère sinon.
	-tabEtuTrier : Le tableau d'étudiant trié.
	-semestre : Le semestre annualisé.
	-wb : Le model excel chargé.
	-today : La date

"""
def ecrireSelonS1S2(preJury, tabEtuTrier, semestre, wb, today):
	#Constante des cases :
	CELL_DEP = 'L4'
	CELL_TITRE = 'M2'
	TITRE = 'JURY de SEMESTRE 2 et d\'ETAPE 1°ANNEE'

	#Constante SN-1
	C_MOY_SN_MOINS_1 = 'F'
	C_NOTES_SN_MOINS_1 = 'J'

	C_RESU_SN_MOINS_1_C = 'K'

	C_RESU_SN_MOINS_1_PJ = 'L'
	C_RESU_SN_MOINS_1_J = 'M'

	#Constante SN
	C_MOY_SN = 'O'
	C_NOTES_SN = 'S'
	C_RESU_SN = 'T'
	C_RESU_SN_PJ = 'U'

	C_RESU_ETAPE = 'Y'

	#Constantes des bordures (les colonnes de départ) :
	BORDURE_ETUDIANT = 'B'
	BORDURE_DEP = 'L'
	BORDURE_S1 = 'F'
	BORDURE_S2 = 'O'
	BORDURE_ETAPE = 'X'

	#Constante Etape année X :
	#ATTENTION ! Ces constantes ne sont valables que pour le doc S1/S2 et S3/S4
	C_MOY_ANNEE = 'X'
	C_MOY_RESU_ANNEE = 'Y'

	#Premiere ligne à remplir
	ligne = 8

	ws = wb.active
	#TODO mettre sous la forme : 30 aout 2017
	ws[CELL_DATE] = today
	ws[CELL_DEP] = DEP
	ws[CELL_TITRE] = TITRE
	ws[CELL_ANNEE_UNIVERSITAIRE] = 'Année ' + str(semestre.anneeUniversitaire)

	i = 1
	for infoEtu in tabEtuTrier:
		ws[C_NUMERO + str(ligne)] = i
		ws[C_NAPOGEE + str(ligne)] = infoEtu.numApogee
		ws[C_NOM + str(ligne)] = infoEtu.nomEtu
		ws[C_PRE + str(ligne)] = infoEtu.prenomEtu

		y = 0
		for moyUE in infoEtu.moyUESNMoins1:
			ws[chr(ord(C_MOY_SN_MOINS_1) + y) + str(ligne)] = moyUE
			y += 1

		ws[C_NOTES_SN_MOINS_1 + str(ligne)] = infoEtu.moySNMoins1
		ws[C_RESU_SN_MOINS_1_C + str(ligne)] = infoEtu.resultatSNMoins1C
		ws[C_RESU_SN_MOINS_1_PJ + str(ligne)] = infoEtu.resultatSNMoins1PJ
		ws[C_RESU_SN_MOINS_1_J + str(ligne)] = infoEtu.resultatSNMoins1J

		#Si ce n'est pas un prejurry, on génère le résultat pré-jury. et on se base sur la colonne préjurry pour le res étape.
		if(not preJury):
			ws[C_RESU_SN_PJ + str(ligne)] = infoEtu.resultatSNPJ
			#1 car on veut le résultat de la 1ere année.
			ws[C_RESU_ETAPE + str(ligne)] = calculerResEtape(1, infoEtu.resultatSNMoins1J, infoEtu.resultatSNPJ)
		else:
			ws[C_RESU_ETAPE + str(ligne)] = calculerResEtape(1, infoEtu.resultatSNMoins1J, infoEtu.resultatSNPJ)

		y = 0
		for moyUE in infoEtu.moyUESN:
			ws[chr(ord(C_MOY_SN) + y) + str(ligne)] = moyUE
			y += 1

		ws[C_NOTES_SN + str(ligne)] = infoEtu.moySN
		ws[C_RESU_SN + str(ligne)] = infoEtu.resultatSN
		ws[C_MOY_ANNEE + str(ligne)] = infoEtu.moyAnnee

		i += 1
		ligne += 1

	#Gestion des bordures :
	faireBordure(BORDURE_ETUDIANT,6,3,ws)
	faireBordure(BORDURE_DEP,4,9,ws)
	faireBordure(BORDURE_S1,6,8,ws)
	faireBordure(BORDURE_S2,6,8,ws)
	#Cas particulier :
	bordureEpaisse = Border(top=Side(style='thick'), bottom=Side(style='thick'), left=Side(style='thick'), right=Side(style='thick'))
	faireBordure(BORDURE_ETAPE,6,3,ws)
	ws['AA6'].border = bordureEpaisse
	ws['AB6'].border = bordureEpaisse
	ws['AB7'].border = bordureEpaisse


	#sauvegarde du fichier généré
	return sauvegarderJury(wb,preJury,semestre)
"""
Va permettre d'écrire dans le fichier Selon S2S3
Paramètre :
	-preJury : Booleen
		-Si true on ne génère pas le résultat préjury, on génère sinon.
	-tabEtuTrier : Le tableau d'étudiant trié.
	-semestre : Le semestre annualisé.
	-wb : Le model excel chargé.
	-today : La date
"""
def ecrireSelonS2S3(preJury, tabEtuTrier, semestre, wb, today):
	#Constante des cases :
	CELL_DEP = 'K4'
	CELL_TITRE = 'N2'
	TITRE = 'JURY de SEMESTRE 3 - BILAN S2 - S3'


	#Constante SN-1
	C_MOY_SN_MOINS_1 = 'F'
	C_NOTES_SN_MOINS_1 = 'J'
	C_MOY_S1S2 = 'K'

	C_RESU_SN_MOINS_1_C = 'L'

	C_RESU_SN_MOINS_1_PJ = 'M'
	C_RESU_SN_MOINS_1_J = 'N'

	C_SITUATION_JANVIER = 'Z6'

	#Constante SN
	C_MOY_SN = 'P'
	C_NOTES_SN = 'T'
	C_MOY_S2S3 = 'U'
	C_RESU_SN = 'V'
	C_RESU_SN_PJ = 'W'


	#Constantes des bordures (les colonnes de départ) :
	BORDURE_ETUDIANT = 'B'
	BORDURE_DEP = 'K'
	BORDURE_S2 = 'F'
	BORDURE_S3 = 'P'
	BORDURE_ETAPE = 'Z'

	tabDate = semestre.anneeUniversitaire.anneeUniversitaire.split('/')
	annee = tabDate[1]

	#Premiere ligne à remplir
	ligne = 8

	ws = wb.active

	ws[CELL_TITRE] = TITRE
	ws[C_SITUATION_JANVIER] = 'Situat. Janvier ' + str(annee)
	ws[CELL_DATE] = today
	ws[CELL_DEP] = DEP
	ws[CELL_ANNEE_UNIVERSITAIRE] = 'Année ' + str(semestre.anneeUniversitaire)

	i = 1
	for infoEtu in tabEtuTrier:
		ws[C_NUMERO + str(ligne)] = i
		ws[C_NAPOGEE + str(ligne)] = infoEtu.numApogee
		ws[C_NOM + str(ligne)] = infoEtu.nomEtu
		ws[C_PRE + str(ligne)] = infoEtu.prenomEtu

		y = 0
		for moyUE in infoEtu.moyUESNMoins1:
			ws[chr(ord(C_MOY_SN_MOINS_1) + y) + str(ligne)] = moyUE
			y += 1

		ws[C_NOTES_SN_MOINS_1 + str(ligne)] = infoEtu.moySNMoins1
		ws[C_MOY_S1S2 + str(ligne)] = infoEtu.moyS1S2
		ws[C_RESU_SN_MOINS_1_C + str(ligne)] = infoEtu.resultatSNMoins1C
		ws[C_RESU_SN_MOINS_1_PJ + str(ligne)] = infoEtu.resultatSNMoins1PJ
		ws[C_RESU_SN_MOINS_1_J + str(ligne)] = infoEtu.resultatSNMoins1J

		#Si ce n'est pas un prejurry, on génère le résultat pré-jury.
		if(not preJury):
			ws[C_RESU_SN_PJ + str(ligne)] = infoEtu.resultatSNPJ

		y = 0
		for moyUE in infoEtu.moyUESN:
			ws[chr(ord(C_MOY_SN) + y) + str(ligne)] = moyUE
			y += 1

		ws[C_NOTES_SN + str(ligne)] = infoEtu.moySN
		ws[C_MOY_S2S3 + str(ligne)] = infoEtu.moyS2S3
		ws[C_RESU_SN + str(ligne)] = infoEtu.resultatSN

		i += 1
		ligne += 1

	#Gestion des bordures :
	faireBordure(BORDURE_ETUDIANT,6,3,ws)
	faireBordure(BORDURE_DEP,4,12,ws)
	faireBordure(BORDURE_S2,6,9,ws)
	faireBordure(BORDURE_S3,6,9,ws)

	#Cas particulier
	bordureEpaisse = Border(top=Side(style='thick'), bottom=Side(style='thick'), left=Side(style='thick'), right=Side(style='thick'))
	ws[BORDURE_ETAPE + str(6)].border = bordureEpaisse
	ws[BORDURE_ETAPE + str(7)].border = bordureEpaisse

	#sauvegarde du fichier généré
	return sauvegarderJury(wb,preJury,semestre)

	#ws = wb.active


"""
Va permettre d'écrire dans le fichier Selon S3S4
Paramètre :
	-preJury : Booleen
	Si true on ne génère pas le résultat préjury, on génère sinon.
	-tabEtuTrier : Le tableau d'étudiant trié.
	-semestre : Le semestre annualisé.
	-wb : Le model excel chargé.
	-today : La date
"""
def ecrireSelonS3S4(preJury,tabEtuTrier,semestre,wb,today):
	#Constante des cases :
	CELL_DEP = 'L4'
	CELL_TITRE = 'M2'
	TITRE = 'JURY de SEMESTRE 4 et d\'ETAPE 2°ANNEE'
	C_SITUATION_SEPTEMBRE = 'AB6'

	#Constante SN-1
	C_MOY_SN_MOINS_1 = 'F'
	C_NOTES_SN_MOINS_1 = 'J'

	C_RESU_SN_MOINS_1_C = 'K'

	C_RESU_SN_MOINS_1_PJ = 'L'
	C_RESU_SN_MOINS_1_J = 'M'

	#Constante SN
	C_MOY_SN = 'O'
	C_NOTES_SN = 'S'
	C_RESU_SN = 'T'
	C_RESU_SN_PJ = 'U'
	C_MOY_ANNEE = 'X'
	C_RESU_ETAPE = 'Y'

	#Constantes des bordures (les colonnes de départ) :
	BORDURE_ETUDIANT = 'B'
	BORDURE_DEP = 'L'
	BORDURE_S3 = 'F'
	BORDURE_S4 = 'O'
	BORDURE_ETAPE = 'X'

	try:
		tabDate = today.split('-')
		annee = tabDate[0]
	except:
		try:
			tabDate = today.split('/')
			annee = tabDate[2]
		except:
			titreErreur = 'Erreur dans la génération du fichier !'
			messageErreur = 'La date est incorrect.'
			detailsErreur = []
			detailsErreur.append('Avez-vous bien remplie la date au format jj/mm/aaaa si vous êtes sous mozilla firefox ?')
			detailsErreur.append('Avez-vous sélectionnez une date correct si vous êtes sous google chrome ?')
			return render(request, 'erreurUtilisateur.html', locals())


	#Premiere ligne à remplir
	ligne = 8

	ws = wb.active
	ws[C_SITUATION_SEPTEMBRE] = 'Situat. Sept. ' + str(annee)
	ws[CELL_TITRE] = TITRE
	ws[CELL_DATE] = today
	ws[CELL_DEP] = DEP
	ws[CELL_ANNEE_UNIVERSITAIRE] = 'Année ' + str(semestre.anneeUniversitaire)

	i = 1
	for infoEtu in tabEtuTrier:
		ws[C_NUMERO + str(ligne)] = i
		ws[C_NAPOGEE + str(ligne)] = infoEtu.numApogee
		ws[C_NOM + str(ligne)] = infoEtu.nomEtu
		ws[C_PRE + str(ligne)] = infoEtu.prenomEtu

		y = 0
		for moyUE in infoEtu.moyUESNMoins1:
			ws[chr(ord(C_MOY_SN_MOINS_1) + y) + str(ligne)] = moyUE
			y += 1

		ws[C_NOTES_SN_MOINS_1 + str(ligne)] = infoEtu.moySNMoins1
		ws[C_RESU_SN_MOINS_1_C + str(ligne)] = infoEtu.resultatSNMoins1C
		ws[C_RESU_SN_MOINS_1_PJ + str(ligne)] = infoEtu.resultatSNMoins1PJ
		ws[C_RESU_SN_MOINS_1_J + str(ligne)] = infoEtu.resultatSNMoins1J

		#Si ce n'est pas un prejurry, on génère le résultat pré-jury. et on se base sur la colonne préjurry pour le res étape.
		if(not preJury):
			ws[C_RESU_SN_PJ + str(ligne)] = infoEtu.resultatSNPJ
			#1 car on veut le résultat de la 1ere année.
			ws[C_RESU_ETAPE + str(ligne)] = calculerResEtape(1, infoEtu.resultatSNMoins1J, infoEtu.resultatSNPJ)
		else:
			ws[C_RESU_ETAPE + str(ligne)] = calculerResEtape(1, infoEtu.resultatSNMoins1J, infoEtu.resultatSNPJ)

		y = 0
		for moyUE in infoEtu.moyUESN:
			ws[chr(ord(C_MOY_SN) + y) + str(ligne)] = moyUE
			y += 1

		ws[C_NOTES_SN + str(ligne)] = infoEtu.moySN
		ws[C_RESU_SN + str(ligne)] = infoEtu.resultatSN
		ws[C_MOY_ANNEE + str(ligne)] = infoEtu.moyAnnee

		i += 1
		ligne += 1

	faireBordure(BORDURE_ETUDIANT,6,3,ws)
	faireBordure(BORDURE_DEP,4,9,ws)
	faireBordure(BORDURE_S3,6,8,ws)
	faireBordure(BORDURE_S4,6,8,ws)

	#Cas particulier
	bordureEpaisse = Border(top=Side(style='thick'), bottom=Side(style='thick'), left=Side(style='thick'), right=Side(style='thick'))
	faireBordure(BORDURE_ETAPE,6,3,ws)
	ws['AA6'].border = bordureEpaisse
	ws['AB6'].border = bordureEpaisse
	ws['AB7'].border = bordureEpaisse

	#sauvegarde du fichier généré
	return sauvegarderJury(wb,preJury,semestre)

	ws = wb.active
"""
Fonction qui gère l'écriture du doc Jury proprement dite
Paramètre:
	-wb : Workbook avec toutes les informations déjà écrites
	-Préjury : True si c'est un doc Préjury, False sinon
	-Semestre : semestre annualisé concerné par le document
Retourne:
	Path du fichier généré au format String
"""
def sauvegarderJury(wb,preJury,semestre):
	try:
		if preJury:
			chemin = '/home/iut/Bureau/Docs_Ageil/'+str(semestre.anneeUniversitaire).replace('/','_')+'/'+semestre.semestre.semestre+'/PJury/'
			cheminApp = 'excel/fichierGenerer/pre_jury/'

		else:
			chemin = '/home/iut/Bureau/Docs_Ageil/'+str(semestre.anneeUniversitaire).replace('/','_')+'/'+semestre.semestre.semestre+'/Jury/'
			cheminApp = 'excel/fichierGenerer/jury/'

		if not(os.path.exists(chemin)):
			os.makedirs(chemin)

		if datetime.datetime.now().day < 10:
			jour = '0'+str(datetime.datetime.now().day)
		else:
			jour = str(datetime.datetime.now().day)

		if datetime.datetime.now().month < 10:
			mois = '0'+str(datetime.datetime.now().month)
		else:
			mois = str(datetime.datetime.now().month)

		annee = str(datetime.datetime.now().year)

		if datetime.datetime.now().hour < 10:
			heure = '0'+str(datetime.datetime.now().hour)
		else:
			heure = str(datetime.datetime.now().hour)

		if datetime.datetime.now().minute < 10:
			minute = '0'+str(datetime.datetime.now().minute)
		else:
			minute = str(datetime.datetime.now().minute)


		if preJury:
			fichier = chemin+'PJury__'+str(semestre.anneeUniversitaire).replace('/','_')+'__'+semestre.semestre.semestre+'__'+jour+mois+annee+'_'+heure+minute+'.xlsx'
			fichierApp = cheminApp + 'preJury_'+semestre.semestre.semestre+'.xlsx'
		else:
			fichier = chemin+'Jury__'+str(semestre.anneeUniversitaire).replace('/','_')+'__'+semestre.semestre.semestre+'__'+jour+mois+annee+'_'+heure+minute+'.xlsx'
			fichierApp = cheminApp + 'jury_'+semestre.semestre.semestre+'.xlsx'

		wb.save(fichier)
		#On garde au cas où la génération du doc dans l'appli (elle ecrasera la precedente)
		wb.save(fichierApp)
	except:
		if preJury:
			chemin = 'excel/fichierGenerer/pre_jury/'
			fichier = chemin + 'preJury_'+semestre.semestre.semestre+'.xlsx'
		else:
			chemin = 'excel/fichierGenerer/jury/'
			fichier = chemin + 'jury_'+semestre.semestre.semestre+'.xlsx'
		wb.save(fichier)

	return fichier


"""
Permet de faire les bordures autours des cases fusionnées.
Paramètres:
	-col : La colonne de départ.
	-ligne : Le numéro de ligne.
	-nbCases : Le nombre de cases qui sont fusionnées.
	-ws le workbook chargé auparavant.
"""
def faireBordure(col, ligne, nbCases, ws):
		bordureEpaisse = Border(top=Side(style='thick'), bottom=Side(style='thick'), left=Side(style='thick'), right=Side(style='thick'))
		for i in range(0,nbCases):
			ws[chr(ord(col) + i) + str(ligne)].border = bordureEpaisse

"""
Fonction qui permet de générer le document pour le S1.
Paramètre :
	-nomPresidentJury : Le nom du président du jury.
	-semestre : Le semestre Annualisé
	-dateChoisi : La date choisie par l'utilisateur.
"""
def genererPVS1(nomPresidentJury, semestre, dateChoisi):
	#Variables et constantes de cellules
	ligne = 11
	COL_NOM_VAL = 'B'
	COL_PRE_VAL = 'C'

	COL_PRE_NVAL = 'B'
	COL_PRE_NVAL = 'C'
	COL_UE_CAP =  'D'
	COL_CENTRE = 'C'

	DIR_IUT = nomPresidentJury

	CELL_DEP = 'C3'
	CELL_DEP_CONTAIN = 'Département GMP'

	CELL_ANNEE_UNIV = 'C7'
	CELL_ANNEE_UNIV_CONTAIN = 'Année universitaire ' + str(semestre.anneeUniversitaire)

	#Charge le fichier
	wb = choisirFichierSelonSemestre('S1',False)
	#Charge la feuille
	ws = wb.active

	#Ecrit l'en tête du fichier
	ws[CELL_DEP] = CELL_DEP_CONTAIN
	ws[CELL_DEP].alignment = Alignment(horizontal = 'center')
	ws[CELL_ANNEE_UNIV] = CELL_ANNEE_UNIV_CONTAIN
	ws[CELL_ANNEE_UNIV].alignment = Alignment(horizontal = 'center')


	#Charge la liste des étudiants ayant validé
	etudiants_val = getEtudiantValide(semestre)

	#Ecrit l'en tête de la partie des étudiants ayant validé
	ws[COL_NOM_VAL + str(ligne)] = 'NOM'
	ws[COL_NOM_VAL + str(ligne)].font = Font(bold=True)
	ws[COL_NOM_VAL + str(ligne)].border = Border(top=Side(style='thin'), bottom=Side(style='thin'), left=Side(style='thin'), right=Side(style='thin'))

	ws[COL_PRE_VAL + str(ligne)] = 'PRENOM'
	ws[COL_PRE_VAL + str(ligne)].font = Font(bold=True)
	ws[COL_PRE_VAL + str(ligne)].border = Border(top=Side(style='thin'), bottom=Side(style='thin'), right=Side(style='thin'))

	ligne += 1

	#Ecrit la liste des étudiants ayant validé
	for etu in etudiants_val:
		ws[COL_NOM_VAL + str(ligne)] = etu.etudiant.nom
		ws[COL_NOM_VAL + str(ligne)].border = Border(bottom=Side(style='thin'), left=Side(style='thin'), right=Side(style='thin'))

		ws[COL_PRE_VAL + str(ligne)] = etu.etudiant.prenom
		ws[COL_PRE_VAL + str(ligne)].border = Border(bottom=Side(style='thin'), right=Side(style='thin'))
		ligne += 1



	ligne += 1

	#Ecrit la liste des étudiants n'ayant pas validé et leurs UEs capitalisées
	ligne = ecrireUECapitalisees(semestre, ws, COL_NOM_VAL, COL_PRE_VAL, COL_UE_CAP, ligne,COL_UE_CAP)


	ligne += 5

	ecrireFooterPV(ws,dateChoisi, COL_CENTRE, ligne, DIR_IUT)

	return sauvegarderPV(wb,semestre)

"""
Fonction qui permet de générer le document pour le S2.
Paramètre :
	-nomPresidentJury : Le nom du président du jury.
	-semestre : Le semestre Annualisé
	-dateChoisi : La date choisie par l'utilisateur.
Retourne :
	nom du fichier écrit
"""
def genererPVS2(nomPresidentJury, semestre, dateChoisi):
	#Variables et constances nécéssaires à l'écriture
	ligne = 11
	COL_NOM = 'A'
	COL_PRE = 'B'
	COL_SIT = 'C'
	COL_UE_CAP = 'C'
	COL_DEC_S1 = 'D'
	COL_DEC_S2 = 'E'
	COL_CENTRE = 'C'
	DIR_IUT = nomPresidentJury

	CELL_DEP = 'C4'
	CELL_DEP_CONTAIN = 'Département GMP'

	CELL_ANNEE_UNIV = 'C7'
	CELL_ANNEE_UNIV_CONTAIN = 'Année universitaire ' + str(semestre.anneeUniversitaire)


	border = Border(bottom=Side(style='thin'), left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'))
	SP = SemestrePossible.objects.get(semestre = 'S1')
	S1 = SemestreAnnualise.objects.filter(anneeUniversitaire = semestre.anneeUniversitaire).get(semestre = SP)

	wb = choisirFichierSelonSemestre('S2',False)
	ws = wb.active

	#Ecriture de l'en-tête
	ws[CELL_DEP] = CELL_DEP_CONTAIN
	ws[CELL_DEP].alignment = Alignment(horizontal = 'center')
	ws[CELL_ANNEE_UNIV] = CELL_ANNEE_UNIV_CONTAIN
	ws[CELL_ANNEE_UNIV].alignment = Alignment(horizontal = 'center')

	#Ecriture des étudiants
	for etu in getEtudiant(semestre):
		ws[COL_NOM + str(ligne)] = etu.etudiant.nom
		ws[COL_NOM + str(ligne)].border = border

		ws[COL_PRE + str(ligne)] = etu.etudiant.prenom
		ws[COL_PRE + str(ligne)].border = border

		if etu.devenirEtudiant.code != 'ATTENTE_DE_DECISION':
			ws[COL_SIT + str(ligne)] = etu.devenirEtudiant.code
		ws[COL_SIT + str(ligne)].border = border

		ws[COL_DEC_S1 + str(ligne)] = dictValidation()[Association_Etudiant_SemestreAnnualise.objects.filter(semestre = S1).get(etudiant = etu.etudiant).resultatSemestreJury.codeResultat]
		ws[COL_DEC_S1 + str(ligne)].border = border

		ws[COL_DEC_S2 + str(ligne)] = dictValidation()[etu.resultatSemestreJury.codeResultat]
		ws[COL_DEC_S2 + str(ligne)].border = border

		ligne += 1
	ligne += 1

	#Ecriture des étudiants n'ayant pas validé mais ayant capitalisé des UEs
	ligne = ecrireUECapitalisees(semestre, ws, COL_NOM, COL_PRE, COL_UE_CAP, ligne,chr(ord(COL_UE_CAP) + 1))

	ligne += 5

	ecrireFooterPV(ws,dateChoisi, COL_CENTRE, ligne, DIR_IUT)

	return sauvegarderPV(wb,semestre)



"""
Fonction qui permet de générer le document pour le S3.
Paramètre :
	-nomPresidentJury : Le nom du président du jury.
	-semestre : Le semestre Annualisé
	-dateChoisi : La date choisie par l'utilisateur.
"""
def genererPVS3(nomPresidentJury, semestre, dateChoisi):
	#Variables et constances nécéssaires à l'écriture
	ligne = 11
	COL_NOM = 'A'
	COL_PRE = 'B'
	COL_SIT = 'C'
	COL_UE_CAP = 'C'
	COL_DEC_S1 = 'D'
	COL_DEC_S2 = 'E'
	COL_CENTRE = 'C'
	DIR_IUT = nomPresidentJury

	CELL_DEP = 'C4'
	CELL_DEP_CONTAIN = 'Département GMP'

	CELL_ANNEE_UNIV = 'C7'
	CELL_ANNEE_UNIV_CONTAIN = 'Année universitaire ' + str(semestre.anneeUniversitaire)
	border = Border(bottom=Side(style='thin'), left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'))

	#on récupère un semestre de l'année précédente ! il faut commencer par récupérer cette année précédente
	anneeUniversitaireActuel = semestre.anneeUniversitaire.anneeUniversitaire
	annee1,annee2 = anneeUniversitaireActuel.split('/')
	annee1 = int(annee1) - 1
	annee2 = int(annee2) - 1
	anneeUniversitaireNmoins1String = str(annee1) + '/' + str(annee2)

	AUNM1 = AnneeUniversitaire.objects.get(anneeUniversitaire = anneeUniversitaireNmoins1String)
	SP = SemestrePossible.objects.get(semestre = 'S2')
	S1 = SemestreAnnualise.objects.filter(anneeUniversitaire = AUNM1).get(semestre = SP)

	wb = choisirFichierSelonSemestre('S3',False)
	ws = wb.active

	#Ecriture de l'en-tête
	ws[CELL_DEP] = CELL_DEP_CONTAIN
	ws[CELL_DEP].alignment = Alignment(horizontal = 'center')
	ws[CELL_ANNEE_UNIV] = CELL_ANNEE_UNIV_CONTAIN
	ws[CELL_ANNEE_UNIV].alignment = Alignment(horizontal = 'center')

	#Ecriture des étudiants
	for etu in getEtudiant(semestre):
		ws[COL_NOM + str(ligne)] = etu.etudiant.nom
		ws[COL_NOM + str(ligne)].border = border

		ws[COL_PRE + str(ligne)] = etu.etudiant.prenom
		ws[COL_PRE + str(ligne)].border = border

		if etu.devenirEtudiant.code != 'ATTENTE_DE_DECISION':
			ws[COL_SIT + str(ligne)] = etu.devenirEtudiant.code
		ws[COL_SIT + str(ligne)].border = border

		ws[COL_DEC_S1 + str(ligne)] = dictValidation()[Association_Etudiant_SemestreAnnualise.objects.filter(semestre = S1).get(etudiant = etu.etudiant).resultatSemestreJury.codeResultat]
		ws[COL_DEC_S1 + str(ligne)].border = border

		ws[COL_DEC_S2 + str(ligne)] = dictValidation()[etu.resultatSemestreJury.codeResultat]
		ws[COL_DEC_S2 + str(ligne)].border = border

		ligne += 1
	ligne += 1

	ligne += 5

	ecrireFooterPV(ws,dateChoisi, COL_CENTRE, ligne, DIR_IUT)

	return sauvegarderPV(wb,semestre)

"""
Fonction qui permet de générer le document pour le S4.
Paramètre :
	-nomPresidentJury : Le nom du président du jury.
	-semestre : Le semestre Annualisé
	-dateChoisi : La date choisie par l'utilisateur.
"""
def genererPVS4(nomPresidentJury, semestre, dateChoisi):
	#Variables et constances nécéssaires à l'écriture
	ligne = 11
	COL_NOM = 'A'
	COL_PRE = 'B'
	COL_SIT = 'C'
	COL_UE_CAP = 'C'
	COL_DEC_S1 = 'D'
	COL_DEC_S2 = 'E'
	COL_CENTRE = 'C'
	DIR_IUT = nomPresidentJury

	CELL_DEP = 'C4'
	CELL_DEP_CONTAIN = 'Département GMP'

	CELL_ANNEE_UNIV = 'C7'
	CELL_ANNEE_UNIV_CONTAIN = 'Année universitaire ' + str(semestre.anneeUniversitaire)


	border = Border(bottom=Side(style='thin'), left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'))
	SP = SemestrePossible.objects.get(semestre = 'S3')
	S1 = SemestreAnnualise.objects.filter(anneeUniversitaire = semestre.anneeUniversitaire).get(semestre = SP)

	wb = choisirFichierSelonSemestre('S4',False)
	ws = wb.active

	#Ecriture de l'en-tête
	ws[CELL_DEP] = CELL_DEP_CONTAIN
	ws[CELL_DEP].alignment = Alignment(horizontal = 'center')
	ws[CELL_ANNEE_UNIV] = CELL_ANNEE_UNIV_CONTAIN
	ws[CELL_ANNEE_UNIV].alignment = Alignment(horizontal = 'center')

	#Ecriture des étudiants
	for etu in getEtudiant(semestre):
		ws[COL_NOM + str(ligne)] = etu.etudiant.nom
		ws[COL_NOM + str(ligne)].border = border

		ws[COL_PRE + str(ligne)] = etu.etudiant.prenom
		ws[COL_PRE + str(ligne)].border = border

		if etu.devenirEtudiant.code != 'ATTENTE_DE_DECISION':
			ws[COL_SIT + str(ligne)] = etu.devenirEtudiant.code
		ws[COL_SIT + str(ligne)].border = border

		ws[COL_DEC_S1 + str(ligne)] = dictValidation()[Association_Etudiant_SemestreAnnualise.objects.filter(semestre = S1).get(etudiant = etu.etudiant).resultatSemestreJury.codeResultat]
		ws[COL_DEC_S1 + str(ligne)].border = border

		ws[COL_DEC_S2 + str(ligne)] = dictValidation()[etu.resultatSemestreJury.codeResultat]
		ws[COL_DEC_S2 + str(ligne)].border = border

		ligne += 1
	ligne += 1

	#Ecriture des étudiants n'ayant pas validé mais ayant capitalisé des UEs
	ligne = ecrireUECapitalisees(semestre, ws, COL_NOM, COL_PRE, COL_UE_CAP, ligne,chr(ord(COL_UE_CAP) + 1))

	ligne += 5

	ecrireFooterPV(ws,dateChoisi, COL_CENTRE, ligne, DIR_IUT)

	return sauvegarderPV(wb,semestre)

"""
Fonction qui gère l'écriture du PV proprement dite
Paramètre :
	-wb : le workbook avec les informations à l'intérieur
	-semestre : semestre annualise concerné par le PV
	retourne : nom du fichier généré
"""

def sauvegarderPV(wb,semestre):
	try:
		chemin = '/home/iut/Bureau/Docs_Ageil/'+str(semestre.anneeUniversitaire).replace('/','_')+'/'+semestre.semestre.semestre+'/PV/'
		cheminApp='excel/fichierGenerer/PV/'
		if not(os.path.exists(chemin)):
			os.makedirs(chemin)

		if datetime.datetime.now().day < 10:
			jour = '0'+str(datetime.datetime.now().day)
		else:
			jour = str(datetime.datetime.now().day)

		if datetime.datetime.now().month < 10:
			mois = '0'+str(datetime.datetime.now().month)
		else:
			mois = str(datetime.datetime.now().month)

		annee = str(datetime.datetime.now().year)

		if datetime.datetime.now().hour < 10:
			heure = '0'+str(datetime.datetime.now().hour)
		else:
			heure = str(datetime.datetime.now().hour)

		if datetime.datetime.now().minute < 10:
			minute = '0'+str(datetime.datetime.now().minute)
		else:
			minute = str(datetime.datetime.now().minute)



		fichier = chemin+'PV__'+str(semestre.anneeUniversitaire).replace('/','_')+'__'+semestre.semestre.semestre+'__'+jour+mois+annee+'_'+heure+minute+'.xlsx'
		wb.save(fichier)
		#Sauvegarde du fichier dans l'application.
		fichierApp = cheminApp+'PV'+semestre.semestre.semestre+'.xlsx'
		wb.save(fichierApp)
	except:
		chemin = 'excel/fichierGenerer/PV/'
		fichier = chemin + 'PV'+semestre.semestre.semestre+'.xlsx'
		wb.save(fichier)

	return fichier

"""
Fonction qui va choisir le modèle de fichier à utiliser selon le semestre.
Paramètre :
	-semestre : Le semestre à tester (chaine de caractère).
	-pourJury : Si True on va chercher un modèle de doc pour le jury,
				on prend un modèle PV sinon.
	Retourne : Le Workbook chargé.
"""
def choisirFichierSelonSemestre(semestre, pourJury):
	try:
		if(pourJury):
			if(semestre == 'S2'):
				return load_workbook('excel/model/Maquette_S2.xlsx')
			if(semestre == 'S3'):
				return load_workbook('excel/model/Maquette_S3.xlsx')
			if(semestre == 'S4'):
				return load_workbook('excel/model/Maquette_S4.xlsx')
		else:
			if(semestre == 'S1'):
				return load_workbook('excel/model/PVS1.xlsx')
			if(semestre == 'S2'):
				return load_workbook('excel/model/PVS2.xlsx')
			if(semestre == 'S3'):
				return load_workbook('excel/model/PVS3.xlsx')
			if(semestre == 'S4'):
				return load_workbook('excel/model/PVS4.xlsx')
	except:
		# erreur
		titreErreur = 'Erreur dans la génération du fichier !'
		messageErreur = 'Impossible de trouver le modèle à utiliser !'
		detailsErreur = []
		detailsErreur.append('Vérifiez que le fichier utilisé comme modèle est bien présent dans excel/model')
		return render(request, 'erreurUtilisateur.html', locals())

def choixSemestreAReporter(request):

	return choixSemestreAReporterMessage(request, False)

def choixSemestreAReporterMessage(request, message):

	listeSemestre = SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='DOC_PRE_JURY_GENERE'))
	listeSemestre = listeSemestre | SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='DOC_JURY_GENERE'))
	listeSemestre = listeSemestre | SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='DOC_PV_GENERE'))
	listeSemestre = listeSemestre | SemestreAnnualise.objects.filter(etatSemestre=EtatSemestrePossible.objects.get(etat='MOYENNES_IMPORTEES'), semestre=SemestrePossible.objects.get(semestre='S1'))

	return render(request, 'juryManager/choixSemestreAReporter.html', locals())

def reporterResultat(request):

	return listerResultat(request, request.POST['semestreChoisi'], False)

def listerResultat(request, id, message):

	try:

		semestre = SemestreAnnualise.objects.get(id=id)

	except SemestreAnnualise.DoesNotExist:

		# erreur
		titreErreur = 'Erreur'
		messageErreur = 'Le semestre choisit n\'existe pas.'
		detailsErreur = []
		return render(request, 'erreurUtilisateur.html', locals())

	listeAsso = Association_Etudiant_SemestreAnnualise.objects.filter(semestre=semestre)

	semestreValidable = True
	for asso in listeAsso:
		if(asso.devenirEtudiant.code == 'ATTENTE_DE_DECISION' or asso.resultatSemestrePreJury.codeResultat == 'ATTENTE_DE_DECISION' or asso.resultatSemestreJury.codeResultat == 'ATTENTE_DE_DECISION'):
			semestreValidable = False
			messageSemestrePasValidable = "Ce semestre n'est pas validable car certains étudiant sont en attente d'attribution d'une affectaion pour le semestre prochain."

	if(semestreValidable and semestre.etatSemestre.etat != 'DOC_PV_GENERE'):
		semestreValidable = False

		messageSemestrePasValidable = "Ce semestre n'est pas validable car vous n'avez pas généré le PV."

	return render(request, 'juryManager/reporterResultat.html', locals())

def modifierResultat(request, id):

	try:

		assoc = Association_Etudiant_SemestreAnnualise.objects.get(id=id)

	except Association_Etudiant_SemestreAnnualise.DoesNotExist:

		# erreur
		titreErreur = 'Erreur'
		messageErreur = 'Si vous voyez cette erreur, c\'est que vous avez fait une bourde. Arrêtez d\'utiliser le bouton retour arrière de votre navigateur !'
		detailsErreur = []
		return render(request, 'erreurUtilisateur.html', locals())

	if request.method == 'POST':

		form = Association_Etudiant_SemestreAnnualiseForm(request.POST, instance=assoc)

		if form.is_valid():
			assoc = form.save()

			messageAAfficher = 'Les résultats de l\'étudiant ' + str(assoc.etudiant) + ' on été mis à jour.'

			return listerResultat(request, assoc.semestre.id, messageAAfficher)

	else:

		form = Association_Etudiant_SemestreAnnualiseForm(instance=assoc)

		return render(request, 'juryManager/modifierResultat.html', locals())

def modifierDevenir(request, id):

	try:

		assoc = Association_Etudiant_SemestreAnnualise.objects.get(id=id)

	except Association_Etudiant_SemestreAnnualise.DoesNotExist:

		# erreur
		titreErreur = 'Erreur'
		messageErreur = 'Si vous voyez cette erreur, c\'est que vous avez fait une bourde. Arrêtez d\'utiliser le bouton retour arrière de votre navigateur !'
		detailsErreur = []
		return render(request, 'erreurUtilisateur.html', locals())

	if request.method == 'POST':

		assoc.devenirEtudiant = DevenirEtudiantPossible.objects.get(id=request.POST['devenir'])
		assoc.save()

		messageAAfficher = 'Le devenir de l\'étudiant ' + str(assoc.etudiant) + ' a été mis à jour'

		return listerResultat(request, assoc.semestre.id, messageAAfficher)

	else:

		listeDevenirPossible = getListeDevenirPossibleEnFonctionDeLassoc(assoc)
		devenirParDefaut = assoc.devenirEtudiant.code

		return render(request, 'juryManager/modifierDevenir.html', locals())

def getListeDevenirPossibleEnFonctionDeLassoc(assoc):

	resultatJury = assoc.resultatSemestreJury.codeResultat
	typeSemestre = assoc.semestre.semestre.semestre

	listeDevenirPossible = []
	listeDevenirPossible.append(DevenirEtudiantPossible.objects.get(code='Réo'))

	# cas du semestre 1
	if(typeSemestre == 'S1'):

		listeDevenirPossible.append(DevenirEtudiantPossible.objects.get(code='S2'))

	# cas du semestre 2
	if(typeSemestre == 'S2'):

		listeDevenirPossible.append(DevenirEtudiantPossible.objects.get(code='S3'))
		listeDevenirPossible.append(DevenirEtudiantPossible.objects.get(code='S1'))

	# cas du semestre 3
	if(typeSemestre == 'S3'):

		listeDevenirPossible.append(DevenirEtudiantPossible.objects.get(code='S4'))
		listeDevenirPossible.append(DevenirEtudiantPossible.objects.get(code='S2'))

	# cas du semestre 4
	if(typeSemestre == 'S4'):

		listeDevenirPossible.append(DevenirEtudiantPossible.objects.get(code='DUT'))
		listeDevenirPossible.append(DevenirEtudiantPossible.objects.get(code='S4'))
		listeDevenirPossible.append(DevenirEtudiantPossible.objects.get(code='S3'))

	return listeDevenirPossible

def validerSemestre(request, id):

	# on récupère le semestre à valider
	try:

		semestre = SemestreAnnualise.objects.get(id=id)
		typeSemestreActuel = semestre.semestre.semestre

	except SemestreAnnualise.DoesNotExist:

		# erreur
		titreErreur = 'Erreur lors de la validation'
		messageErreur = 'Le semestre choisit n\'existe pas.'
		detailsErreur = []
		return render(request, 'erreurUtilisateur.html', locals())

	# on demande confirmation
	if request.method == 'POST' and 'formulaireDeConfirmation' in request.POST:

		messageAAfficher = 'Les résultats des étudiants du semestre ' + str(semestre) + ' ont bien été validés.'

		# on récupère toutes les Association_Etudiant_SemestreAnnualise qui sont en lien avec ce semestre
		listeAssoc = Association_Etudiant_SemestreAnnualise.objects.filter(semestre=semestre)

		# on calcule l'annee N plus 1 si on en a besoin pour après
		anneeUniversitaireActuel = semestre.anneeUniversitaire
		anneeUniversitaireActuelString = anneeUniversitaireActuel.anneeUniversitaire
		annee1,annee2 = anneeUniversitaireActuelString.split('/')
		annee1 = int(annee1) + 1
		annee2 = int(annee2) + 1
		anneeUniversitaireNplus1String = str(annee1) + '/' + str(annee2)

		try:
			anneeUniversitaireNplus1 = AnneeUniversitaire.objects.get(anneeUniversitaire=anneeUniversitaireNplus1String)

		except AnneeUniversitaire.DoesNotExist:

			anneeUniversitaireNplus1 = AnneeUniversitaire(anneeUniversitaire=anneeUniversitaireNplus1String)
			anneeUniversitaireNplus1.save()

		for assoc in listeAssoc:

			devenir = assoc.devenirEtudiant.code
			resultatN = assoc.resultatSemestreJury.codeResultat
			etudiant = assoc.etudiant

			if(devenir == 'ATTENTE_DE_DECISION'):

				# erreur
				titreErreur = 'Erreur lors de la validation'
				messageErreur = 'L\'un des étudiants n\'a pas d\'affectation renseigné, vous n\'auriez pas du pouvoir valider le semestre.'
				detailsErreur = []
				return render(request, 'erreurUtilisateur.html', locals())

			elif(devenir == 'Réo'):

				if(etudiant.statut.statut == 'redoublant'):
					etudiant.statut = StatutEtudiant.objects.get(statut='redoublant réorienté')
				else:
					etudiant.statut = StatutEtudiant.objects.get(statut='réorienté')
				etudiant.semestreActuel = None

			elif(devenir == 'DUT'):

				if(etudiant.statut.statut == 'redoublant'):
					etudiant.statut = StatutEtudiant.objects.get(statut='redoublant diplomé')
				else:
					etudiant.statut = StatutEtudiant.objects.get(statut='diplomé')
				etudiant.semestreActuel = None

			# cas de poursuite vers un semestre bien définit
			else:

				semestreP = SemestrePossible.objects.get(semestre=devenir)

				if(typeSemestreActuel == 'S1' or typeSemestreActuel == 'S3'):

					anneeUni = AnneeUniversitaire.objects.get(anneeUniversitaire=anneeUniversitaireActuelString)

				elif(typeSemestreActuel == 'S2' or typeSemestreActuel == 'S4'):

					anneeUni = anneeUniversitaireNplus1

				try:

					semestreSuivant = SemestreAnnualise.objects.get(semestre=semestreP, anneeUniversitaire=anneeUni)

				except SemestreAnnualise.DoesNotExist:

					semestreSuivant = SemestreAnnualise(semestre=semestreP, anneeUniversitaire=anneeUni, modeleFormation=semestre.modeleFormation, etatSemestre=EtatSemestrePossible.objects.get(etat='EN_COURS'))
					semestreSuivant.save()

				etudiant.semestreActuel = semestreSuivant

				# on récupère une liste des ModeleSemestre du ModeleFormation que l'on attribut à l'étudiant du typeSemestre auquel l'étudiant est ajouté
				modelesSemestre = ModeleSemestre.objects.filter(modeleFormation=semestre.modeleFormation, typeSemestre=semestreSuivant.semestre)

				# par défaut, on prend le premier élément de cette liste
				modeleSemestre = modelesSemestre[0]

				resultat = ResultatSemestrePossible.objects.get(codeResultat='ATTENTE_DE_DECISION')
				devenirSuivant = DevenirEtudiantPossible.objects.get(code='ATTENTE_DE_DECISION')
				Association_Etudiant_SemestreAnnualise(etudiant=etudiant, semestre=semestreSuivant, modeleSemestre=modeleSemestre, resultatSemestreCalcule=resultat, resultatSemestrePreJury=resultat, resultatSemestreJury=resultat, devenirEtudiant=devenirSuivant).save()

				if(devenir == 'S1' or (typeSemestreActuel == 'S3' and devenir == 'S2') or typeSemestreActuel == 'S4'):

					etudiant.statut = StatutEtudiant.objects.get(statut='redoublant')

			# on modifit le résultat du semestre précédent en fonction de la decision jury si besoin ...

			if(typeSemestreActuel == 'S2'):

				semestreAnnualiseNmoins1 = SemestreAnnualise.objects.get(anneeUniversitaire=assoc.semestre.anneeUniversitaire, semestre=SemestrePossible.objects.get(semestre='S1'))

			if(typeSemestreActuel == 'S3'):

				#on récupère un semestre de l'année précédente ! il faut commencer par récupérer cette année précédente
				anneeUniversitaireActuel = assoc.semestre.anneeUniversitaire.anneeUniversitaire
				annee1,annee2 = anneeUniversitaireActuel.split('/')
				annee1 = int(annee1) - 1
				annee2 = int(annee2) - 1
				anneeUniversitaireNmoins1String = str(annee1) + '/' + str(annee2)

				try:
					anneeUniversitaireNmoins1 = AnneeUniversitaire.objects.get(anneeUniversitaire=anneeUniversitaireNmoins1String)

				except AnneeUniversitaire.DoesNotExist:

					# erreur, pas de S2 et pourtant on veut calculer les résultat du S3
					titreErreur = 'Erreur lors de la validation des résultat'
					messageErreur = 'Les étudiants dont vous voulez importer les moyennes du S3 n\'ont pas de S2 !.'
					detailsErreur = []

					return render(request, 'erreurUtilisateur.html', locals())

				semestreAnnualiseNmoins1 = SemestreAnnualise.objects.get(anneeUniversitaire=anneeUniversitaireNmoins1, semestre=SemestrePossible.objects.get(semestre='S2'))

			if(typeSemestreActuel == 'S4'):

				semestreAnnualiseNmoins1 = SemestreAnnualise.objects.get(anneeUniversitaire=assoc.semestre.anneeUniversitaire, semestre=SemestrePossible.objects.get(semestre='S3'))

			if(typeSemestreActuel != 'S1'):

				assocNmoins1 = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=semestreAnnualiseNmoins1)
				resultatNmoins1 = assocNmoins1.resultatSemestreJury.codeResultat

				if(resultatNmoins1 == 'NATT' or resultatNmoins1 == 'NATB'):

					if(resultatN == 'VAL' or resultatN == 'NATT' or resultatN == 'NATB' or resultatN == 'VALJ'):

						assocNmoins1.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='VALJ')

					if(resultatN == 'VALC'):

						assocNmoins1.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='ADAC')

					if(resultatN == 'ADAC'):

						assocNmoins1.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='VALC')

					if(resultatN == 'NVAL' or resultatN == 'DEF'):

						assocNmoins1.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='NVAL')

			if(typeSemestreActuel != 'S1'):

				assocNmoins1.save()

			etudiant.save()

		# on passe l'etat de semestre à terminé
		semestre.etatSemestre = EtatSemestrePossible.objects.get(etat='TERMINE')
		semestre.save()

		return choixSemestreAReporterMessage(request, messageAAfficher)

	else:

		actionUtilisateur = 'Valider les résultats saisie. Attention ! Cette action vous empêchera de modifier à nouveau les résultats et fera avancer les étudiants dans la formation.'
		lienActionUtilisateur = '/gestionJury/validerSemestre/' + str(semestre.id)
		return render(request, 'pageConfirmation.html', locals())
