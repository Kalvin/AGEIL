# coding: utf8
from django.shortcuts import render
from home.models import Etudiant, ModeleFormation, ModeleSemestre, ModeleUE, ModeleModule, SemestrePossible, SemestreAnnualise, Association_Etudiant_SemestreAnnualise, MoyenneSemestre, BAC, MentionBACPossible, TypeBAC, TypeCursus, CursusPossible, CursusPreDUT, CursusPostDUT
from home.forms import ModuleForm, UEForm, SemestreForm, ModeleForm, BACForm, CursusForm

# Create your views here.
def homeFormation(request):
	return homeFormationMessage(request, False)

def homeFormationMessage(request, messageAAfficher):
	# liste de tout les modeles
	modelesCursusDUT = ModeleFormation.objects.all().order_by('-id')
	
	# on détermine quel sont les models supprimable
	modelesNonSupprimable = set()
	semestresAnnu = SemestreAnnualise.objects.all()
	
	for sem in semestresAnnu:
		modelesNonSupprimable.add(sem.modeleFormation)
	
	form = ModeleForm()
	
	# on affiche
	return render(request, 'enseignementManager/ModelesOnglets.html', locals())
	
def creerTreeViewModeleFormation(request):
	
	form = ModeleForm(request.POST)
	
	if form.is_valid():
		
		try:
			modeleExistant = ModeleFormation.objects.get(nom=request.POST['nom'])
			
			# Erreur, nom de modèle déjà existant.
			titreErreur = 'Erreur de création du modèle.'
			messageErreur = 'Le nom du modèle doit être unique.'
			detailsErreur = []
			detailsErreur.append('Il existe déjà un modèle nommé : \'' + request.POST['nom'] + '\'')
			
			return render(request, 'erreurUtilisateur.html', locals())
			
		except ModeleFormation.DoesNotExist:
			
			# le cursus n'existe pas, on peut le rajouter
			modele = form.save()
			
			#on crée une structure minimale pour le cursus avec au moins les 4 semestres
			
			ModeleSemestre(intitule='Semestre 1', typeSemestre=SemestrePossible.objects.get(semestre='S1'), modeleFormation=modele).save()
			ModeleSemestre(intitule='Semestre 2', typeSemestre=SemestrePossible.objects.get(semestre='S2'), modeleFormation=modele).save()
			ModeleSemestre(intitule='Semestre 3', typeSemestre=SemestrePossible.objects.get(semestre='S3'), modeleFormation=modele).save()
			ModeleSemestre(intitule='Semestre 4', typeSemestre=SemestrePossible.objects.get(semestre='S4'), modeleFormation=modele).save()
			
			semestres = ModeleSemestre.objects.filter(modeleFormation=modele).order_by('typeSemestre')
			
			semestresNonSupprimableEtudiant = []
			
			# on empeche de supprimer un semestre si il est le seul de ce type (il faut toujours au moins 4 semestres)
			semestresNonSupprimableStructureMinimum = []
			for semestre in semestres:
				listeSem = ModeleSemestre.objects.filter(modeleFormation=semestre.modeleFormation, typeSemestre=semestre.typeSemestre)
				if len(listeSem) < 2:
					semestresNonSupprimableStructureMinimum.append(semestre)
			
			# on détermine les semestres dont on peut modifier le structure (aucune moyenne relié à ce semestre)
			semestresStructureNonAlterable = []
			
			UEs = []
			modules = []
			
			return render(request, 'enseignementManager/treeViewModeleFormation.html', locals())
	
def modifierTreeViewModeleFormation(request, idModele):
	
	modele = ModeleFormation.objects.get(id=idModele)
	
	# les semestres qui font partie de ce modele
	semestres = ModeleSemestre.objects.filter(modeleFormation=modele).order_by('typeSemestre')
	
	# on détermine les semestres qui sont supprimable (aucun étudiant n'y est inscrit)
	semestresNonSupprimableEtudiant = []
	assocs = Association_Etudiant_SemestreAnnualise.objects.all()
	for assoc in assocs:
		semestresNonSupprimableEtudiant.append(assoc.modeleSemestre)
	
	# on empeche de supprimer un semestre si il est le seul de ce type (il faut toujours au moins 4 semestres)
	semestresNonSupprimableStructureMinimum = []
	for semestre in semestres:
		listeSem = ModeleSemestre.objects.filter(modeleFormation=semestre.modeleFormation, typeSemestre=semestre.typeSemestre)
		if len(listeSem) < 2:
			semestresNonSupprimableStructureMinimum.append(semestre)
	
	# on détermine les semestres dont on peut modifier le structure (aucune moyenne relié à ce semestre)
	semestresStructureNonAlterable = []
	moyennesSemestre = MoyenneSemestre.objects.all()
	for moy in moyennesSemestre:
		semestresStructureNonAlterable.append(moy.modeleSemestre)
	
	# les UEs qui font partie de ce modele
	UEs = []
	
	for semestre in semestres:
		uesDuSemestre = ModeleUE.objects.filter(modeleSemestre=semestre)
		for ue in uesDuSemestre:
			UEs.append(ue)
	
	# les modules qui font partie de ce modele
	modules = []
	
	for ue in UEs:
		modulesDeUE = ModeleModule.objects.filter(modeleUE=ue)
		for module in modulesDeUE:
			modules.append(module)
	
	return render(request, 'enseignementManager/treeViewModeleFormation.html', locals())

def supprimerModeleFormation(request, idModele):
	
	try:
		modele = ModeleFormation.objects.get(id=idModele)
	except ModeleFormation.DoesNotExist:
		titreErreur = 'Erreur de suppression du modèle de formation'
		messageErreur = 'Le modèle de formation que vous souhaitez supprimer n\'existe pas.'
		detailsErreur = []
		pageRetour = '/gestionEnseignement/formation'
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST' and 'formulaireDeConfirmation' in request.POST:
		
		messageAAfficher = 'Le modèle ' + modele.nom + ' a bien été supprimé'
		modele.delete()
		return homeFormationMessage(request, messageAAfficher)
	
	else:
		
		actionUtilisateur = 'Supprimer le modèle de formation ' + modele.nom
		valeursPOSTAConserver = []
		lienActionUtilisateur = '/gestionEnseignement/supprimerModeleFormation/' + idModele
		return render(request, 'pageConfirmation.html', locals())
	
def paramModifierInformationsModeleFormation(request, idModele):
	
	modele = ModeleFormation.objects.get(id=idModele)
	form = ModeleForm(instance=modele)
	
	return render(request, 'enseignementManager/modifierInformationModeleFormation.html', locals())
	
def modifierInformationsModeleFormation(request):
	
	modele = ModeleFormation.objects.get(id=request.POST['idModele'])
	form = ModeleForm(request.POST, instance=modele)
	
	if form.is_valid():
		form.save()
	
	return modifierTreeViewModeleFormation(request, request.POST['idModele'])

def copierModeleFormation(request, idModele):
	
	# On récupère le modèle de formation à copier
	try:
		modeleFormationOriginal = ModeleFormation.objects.get(id=idModele)
	except ModeleFormation.DoesNotExist:
		titreErreur = 'Erreur de copie du modèle de formation'
		messageErreur = 'Le modèle de formation que vous souhaitez copier n\'existe pas.'
		detailsErreur = []
		pageRetour = '/gestionEnseignement/formation'
		return render(request, 'erreurUtilisateur.html', locals())
	
	# On crée le nouveau modèle de formation
	
	# 1) On cherche le premier nom de copie disponible
	numCopie = 1
	modeleExistant = ModeleFormation.objects.filter(nom=modeleFormationOriginal.nom + '_copie_' + str(numCopie))
	while modeleExistant:
		numCopie = numCopie + 1
		modeleExistant = ModeleFormation.objects.filter(nom=modeleFormationOriginal.nom + '_copie_' + str(numCopie))
	
	# 2) On crée le modèle
	modeleFormationCopie = ModeleFormation(nom=modeleFormationOriginal.nom + '_copie_' + str(numCopie), description=modeleFormationOriginal.description)
	modeleFormationCopie.save()
	
	# On récupère la liste des modèles de semestre du modèle de formation à copier
	listeModelesSemetresOrigine = ModeleSemestre.objects.filter(modeleFormation=modeleFormationOriginal)
	
	# pour chaque semestre du modèle d'origine
	for modeleSemestreOrigine in listeModelesSemetresOrigine:
	
		# On crée une copie du semestre
		modeleSemestreCopie = ModeleSemestre(intitule=modeleSemestreOrigine.intitule, codeApogee=modeleSemestreOrigine.codeApogee, modeleFormation=modeleFormationCopie, typeSemestre=modeleSemestreOrigine.typeSemestre)
		modeleSemestreCopie.save()
		
		# On copie la sous arborescence du semestre
		listeModelesUEsOrigine = ModeleUE.objects.filter(modeleSemestre=modeleSemestreOrigine)
		for modeleUEOrigine in listeModelesUEsOrigine:
			
			modeleUECopie = ModeleUE(code=modeleUEOrigine.code, codeApogee=modeleUEOrigine.codeApogee, intitule=modeleUEOrigine.intitule, coefficient=modeleUEOrigine.coefficient, modeleSemestre=modeleSemestreCopie)
			modeleUECopie.save()
			
			listeModelesModulesOrigine = ModeleModule.objects.filter(modeleUE=modeleUEOrigine)
			for modeleModuleOrigine in listeModelesModulesOrigine:
				
				modeleModuleCopie = ModeleModule(code=modeleModuleOrigine.code, codeApogee=modeleModuleOrigine.codeApogee, intitule=modeleModuleOrigine.intitule, coefficient=modeleModuleOrigine.coefficient, modeleUE=modeleUECopie)
				modeleModuleCopie.save()
	
	return homeFormationMessage(request, 'Le modèle ' + modeleFormationOriginal.nom + ' a été copié sous le nom ' + modeleFormationCopie.nom)
	
def paramModifierSemestre(request, idModele, idSemestre):
	
	semestre = ModeleSemestre.objects.get(id=idSemestre)
	form = SemestreForm(instance=semestre)
	
	return render(request, 'enseignementManager/modifierSemestre.html', locals())
	
def modifierSemestre(request):
	semestre = ModeleSemestre.objects.get(id=request.POST['idSemestre'])
	form = SemestreForm(request.POST, instance=semestre)
	
	if form.is_valid():
		form.save()
	
	return modifierTreeViewModeleFormation(request, request.POST['idModele'])
	
def paramAjouterSemestre(request, idModele):
	
	modele = ModeleFormation.objects.get(id=idModele)
	form = SemestreForm()
	
	return render(request, 'enseignementManager/ajouterSemestre.html', locals())
	
def ajouterSemestre(request):
	print(request.POST)
	form = SemestreForm(request.POST)
	
	if form.is_valid():
		semestre = form.save(commit = False)
		semestre.modeleFormation = ModeleFormation.objects.get(id=request.POST['idModele'])
		semestre.typeSemestre = SemestrePossible.objects.get(semestre=request.POST['typeSemestre'])
		semestre.save()
	
	return modifierTreeViewModeleFormation(request, request.POST['idModele'])
	
def supprimerSemestre(request, idModele, idSemestre):
	
	try:
		modeleSemestre = ModeleSemestre.objects.get(id=idSemestre)
	except ModeleSemestre.DoesNotExist:
		titreErreur = 'Erreur de suppression du modèle de semestre'
		messageErreur = 'Le modèle de semestre que vous souhaitez supprimer n\'existe pas.'
		detailsErreur = []
		pageRetour = '/gestionEnseignement/modifierTreeViewModeleFormation/' + idModele
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST' and 'formulaireDeConfirmation' in request.POST:
	
		modeleSemestre.delete()
		return modifierTreeViewModeleFormation(request, idModele)
	
	else:
		
		actionUtilisateur = 'Supprimer le modèle de semestre ' + modeleSemestre.intitule
		valeursPOSTAConserver = []
		lienActionUtilisateur = '/gestionEnseignement/supprimerSemestre/' + idModele + '/' + idSemestre
		return render(request, 'pageConfirmation.html', locals())
	
def paramModifierUE(request, idModele, idUE):
	
	ue = ModeleUE.objects.get(id=idUE)
	form = UEForm(instance=ue)
	
	return render(request, 'enseignementManager/modifierUE.html', locals())
	
def modifierUE(request):
	ue = ModeleUE.objects.get(id=request.POST['idUE'])
	form = UEForm(request.POST, instance=ue)
	
	if form.is_valid():
		form.save()
	
	return modifierTreeViewModeleFormation(request, request.POST['idModele'])
	
def paramAjouterUE(request, idModele, idSemestre):
	
	semestre = ModeleSemestre.objects.get(id=idSemestre)
	form = UEForm()
	
	return render(request, 'enseignementManager/ajouterUE.html', locals())
	
def ajouterUE(request):
	form = UEForm(request.POST)
	
	if form.is_valid():
		ue = form.save(commit = False)
		ue.modeleSemestre = ModeleSemestre.objects.get(id=request.POST['idSemestre'])
		ue.save()
	
	return modifierTreeViewModeleFormation(request, request.POST['idModele'])
	
def supprimerUE(request, idModele, idUE):
	
	try:
		modeleUE = ModeleUE.objects.get(id=idUE)
	except ModeleUE.DoesNotExist:
		titreErreur = 'Erreur de suppression du modèle d\'UE'
		messageErreur = 'Le modèle d\'UE que vous souhaitez supprimer n\'existe pas.'
		detailsErreur = []
		pageRetour = '/gestionEnseignement/modifierTreeViewModeleFormation/' + idModele
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST' and 'formulaireDeConfirmation' in request.POST:
	
		modeleUE.delete()
		return modifierTreeViewModeleFormation(request, idModele)
	
	else:
		
		actionUtilisateur = 'Supprimer le modèle d\'UE ' + modeleUE.intitule
		valeursPOSTAConserver = []
		lienActionUtilisateur = '/gestionEnseignement/supprimerUE/' + idModele + '/' + idUE
		return render(request, 'pageConfirmation.html', locals())

def paramModifierModule(request, idModele, idModule):
	
	module = ModeleModule.objects.get(id=idModule)
	form = ModuleForm(instance=module)
	
	return render(request, 'enseignementManager/modifierModule.html', locals())
	
def modifierModule(request):
	module = ModeleModule.objects.get(id=request.POST['idModule'])
	form = ModuleForm(request.POST, instance=module)
	
	if form.is_valid():
		form.save()
	
	return modifierTreeViewModeleFormation(request, request.POST['idModele'])
	
def paramAjouterModule(request, idModele, idUE):
	
	ue = ModeleUE.objects.get(id=idUE)
	form = ModuleForm()
	
	return render(request, 'enseignementManager/ajouterModule.html', locals())
	
def ajouterModule(request):
	
	form = ModuleForm(request.POST)
	
	if form.is_valid():
		module = form.save(commit = False)
		module.modeleUE = ModeleUE.objects.get(id=request.POST['idUE'])
		module.save()
	
	return modifierTreeViewModeleFormation(request, request.POST['idModele'])
	
def supprimerModule(request, idModele, idModule):
	
	try:
		modeleModule = ModeleModule.objects.get(id=idModule)
	except ModeleModule.DoesNotExist:
		titreErreur = 'Erreur de suppression du modèle de module'
		messageErreur = 'Le modèle de module que vous souhaitez supprimer n\'existe pas.'
		detailsErreur = []
		pageRetour = '/gestionEnseignement/modifierTreeViewModeleFormation/' + idModele
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST' and 'formulaireDeConfirmation' in request.POST:
	
		modeleModule.delete()
		return modifierTreeViewModeleFormation(request, idModele)
	
	else:
		
		actionUtilisateur = 'Supprimer le modèle de module ' + modeleModule.intitule
		valeursPOSTAConserver = []
		lienActionUtilisateur = '/gestionEnseignement/supprimerModule/' + idModele + '/' + idModule
		return render(request, 'pageConfirmation.html', locals())

def homeBAC(request):
	return homeBACMessage(request, False)

def homeBACMessage(request, messageAAfficher):

	if request.method == 'POST':
		form = BACForm(request.POST)
		
		if form.is_valid():
			
			try:
				bac = BAC.objects.get(intitule=request.POST['intitule'])
				
				# Erreur, bac déjà existant.
				titreErreur = 'Erreur de création du BAC.'
				messageErreur = 'Le nom du BAC doit être unique.'
				detailsErreur = []
				detailsErreur.append('Il existe déjà un BAC nommé : \'' + request.POST['intitule'] + '\'')
				pageRetour = '/gestionEnseignement/BAC'
				return render(request, 'erreurUtilisateur.html', locals())
			
			except BAC.DoesNotExist:
				
				bac = form.save()
				messageAAfficher = 'Le BAC ' + bac.intitule + ' a bien été ajouté.'
	
	etus = Etudiant.objects.all()
	
	listeBACNonSupprimable = []
	
	for etu in etus:
		listeBACNonSupprimable.append(etu.BAC)
	
	form = BACForm()
	
	#On récupère tous les bacs présents dans la BDD en vue d'un affichage
	modelesBAC = BAC.objects.all()
	
	return render(request, 'enseignementManager/ModelesOnglets.html', locals())

def supprimerBAC(request, idBAC):
	#On récupère le BAC dans une variable avant de le supprimer, afin d'avoir accès à son intitule.
	try:
		modeleBAC = BAC.objects.get(id=idBAC)
		
	except BAC.DoesNotExist:
		# Erreur, BAC inexistant.
		titreErreur = 'Erreur de suppression du BAC.'
		messageErreur = 'Le BAC que vous souhaitez supprimer n\'existe pas.'
		detailsErreur = []
		pageRetour = '/gestionEnseignement/BAC'
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST' and 'formulaireDeConfirmation' in request.POST:
		
		messageAAfficher = 'Le BAC ' + modeleBAC.intitule + ' à bien été supprimé.'
		modeleBAC.delete()
		request.method = 'GET'
		return homeBACMessage(request, messageAAfficher)
		
	else:
		
		actionUtilisateur = 'Supprimer le modèle de BAC ' + modeleBAC.intitule
		valeursPOSTAConserver = []
		lienActionUtilisateur = '/gestionEnseignement/supprimerBAC/' + idBAC
		return render(request, 'pageConfirmation.html', locals())

def paramModifierBAC(request, idBAC):
	
	modeleBAC = BAC.objects.get(id=idBAC)
	form = BACForm(instance=modeleBAC)
	return render(request, 'enseignementManager/modifierBAC.html', locals())

def modifierBAC(request):

	modeleBAC = BAC.objects.get(id=request.POST['idBAC'])
	form = BACForm(request.POST, instance=modeleBAC)
	messageAAfficher = 'Le BAC à bien été modifié.'
	if form.is_valid():
		request.method = 'GET'
		form.save()
	
	return homeBACMessage(request, messageAAfficher)
	
"""
Gestion des cursus :
"""
def homeCursus(request):
	return homeCursusMessage(request, False)

def homeCursusMessage(request, messageAAfficher):
	
	if request.method == 'POST': #S'il s'agit d'une requête POST.
		form = CursusForm(request.POST)
		
		if form.is_valid():
			
			try:
				cursusExistant = CursusPossible.objects.get(intitule=request.POST['intitule'])
				
				# Erreur, cursus déjà existant.
				titreErreur = 'Erreur de création du cursus.'
				messageErreur = 'Le nom du cursus doit être unique.'
				detailsErreur = []
				detailsErreur.append('Il existe déjà un cursus nommé : \'' + request.POST['intitule'] + '\'')
				pageRetour = '/gestionEnseignement/cursus'
				return render(request, 'erreurUtilisateur.html', locals())
				
			except CursusPossible.DoesNotExist:
				
				# le cursus n'existe pas, on peut le rajouter
		
				curs = form.save()
				messageAAfficher = 'Le Cursus ' + curs.intitule + ' a bien été ajouté.'
				
	form = CursusForm()
	
	# liste de tous les modeles de cursus
	tabCursus = CursusPossible.objects.all().order_by('-id')
	
	#tableau qui stockera les cursus à ne pas supprimer.
	cursusNonSupprimable = []
	
	#On récupère nos cursus pré/postDUT
	cursPreDUT = CursusPreDUT.objects.all()
	cursusPostDUT = CursusPostDUT.objects.all()
	
	#On test tous nos cursus pour voir lesquels sont supprimables/non supprimables.
	for curs in cursPreDUT:
		cursusNonSupprimable.append(curs.cursus)
		
	for curs in cursusPostDUT:
		cursusNonSupprimable.append(curs.cursus)
	
	return render(request, 'enseignementManager/ModelesOnglets.html', locals())


def supprimerCursus(request, idCursus):
	#On récupère le cursus dans une variable avant de le supprimer, afin d'avoir accès à son intitule.
	try:
		curs = CursusPossible.objects.get(id=idCursus)
		
	except CursusPossible.DoesNotExist:
		# Erreur, cursus inexistant.
		titreErreur = 'Erreur de suppression du cursus.'
		messageErreur = 'Le cursus que vous souhaitez supprimer n\'existe pas.'
		detailsErreur = []
		pageRetour = '/gestionEnseignement/cursus'
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST' and 'formulaireDeConfirmation' in request.POST:
		
		messageAAfficher = 'Le cursus ' + curs.intitule + ' à bien été supprimé.'
		curs.delete()
		request.method = 'GET'
		return homeCursusMessage(request, messageAAfficher)
		
	else:
		
		actionUtilisateur = 'Supprimer le modèle de cursus ' + curs.intitule
		valeursPOSTAConserver = []
		lienActionUtilisateur = '/gestionEnseignement/supprimerCursus/' + idCursus
		return render(request, 'pageConfirmation.html', locals())

def paramModifierCursus(request, idCursus):
	
	cursus = CursusPossible.objects.get(id=idCursus)
	form = CursusForm(instance=cursus)
	
	return render(request, 'enseignementManager/modifierCursus.html', locals())

def modifierCursus(request):
	cursus = CursusPossible.objects.get(id=request.POST['idCursus'])
	form = CursusForm(request.POST, instance=cursus)
	messageAAfficher = 'Le cursus à bien été modifié.'
	if form.is_valid():
		request.method = 'GET'
		form.save()
	
	return homeCursusMessage(request, messageAAfficher)
