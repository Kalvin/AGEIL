from django.contrib import admin
from home.models import Etudiant, StatutEtudiant, Sexe, SemestrePossible, AnneeUniversitaire, SemestreAnnualise, EtatSemestrePossible, Association_Etudiant_SemestreAnnualise, ResultatSemestrePossible, DevenirEtudiantPossible, ModeleFormation, ModeleSemestre, MoyenneSemestre, ModeleUE, ModeleModule, MoyenneUE, MoyenneModule, BAC, MentionBACPossible, TypeBAC, TypeCursus, CursusPossible, CursusPreDUT, CursusPostDUT 

# Register your models here.

admin.site.register(Etudiant)
admin.site.register(StatutEtudiant)
admin.site.register(Sexe)
admin.site.register(SemestrePossible)
admin.site.register(AnneeUniversitaire)
admin.site.register(SemestreAnnualise)
admin.site.register(EtatSemestrePossible)
admin.site.register(Association_Etudiant_SemestreAnnualise)
admin.site.register(ResultatSemestrePossible)
admin.site.register(DevenirEtudiantPossible)
admin.site.register(ModeleFormation)
admin.site.register(ModeleSemestre)
admin.site.register(ModeleUE)
admin.site.register(ModeleModule)
admin.site.register(MoyenneSemestre)
admin.site.register(MoyenneUE)
admin.site.register(MoyenneModule)
admin.site.register(BAC)
admin.site.register(MentionBACPossible)
admin.site.register(TypeBAC)
admin.site.register(TypeCursus)
admin.site.register(CursusPossible)
admin.site.register(CursusPreDUT)
admin.site.register(CursusPostDUT)