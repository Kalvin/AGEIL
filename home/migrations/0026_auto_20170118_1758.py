# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-01-18 16:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0025_auto_20170117_2125'),
    ]

    operations = [
        migrations.AlterField(
            model_name='etatsemestrepossible',
            name='etat',
            field=models.CharField(max_length=50),
        ),
    ]
